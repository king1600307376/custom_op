import math
import matplotlib.pyplot as plt
import numpy as np

type_dic = {"float32": np.float32, "float16": np.float16, "float64": np.float64}


def gen_sin_cos(rad, dtype, length=20):
    sign = 1
    rad = np.mod(rad, np.pi)
    if np.pi / 2 < rad < np.pi:
        rad -= np.pi
        rad = -rad
        sign = -1
    elif -np.pi / 2 < rad < 0:
        rad = -rad
        sign = -1
    elif -np.pi < rad < -np.pi / 2:
        rad += np.pi
    x = type_dic[dtype](1.0)
    y = type_dic[dtype](0.0)
    k = type_dic[dtype](0.6072529350088812564694)
    # at_ls = [1.1071487177940904, 0.7853981633974483, 0.4636476090008061, 0.24497866312686414, 0.12435499454676144,
    #          0.06241880999595735, 0.031239833430268277, 0.015623728620476831, 0.007812341060101111,
    #          0.0039062301319669718,
    #          0.0019531225164788188, 0.0009765621895593195, 0.0004882812111948983, 0.00024414062014936177,
    #          0.00012207031189367021, 6.103515617420877e-05, 3.0517578115526096e-05, 1.5258789061315762e-05,
    #          7.62939453110197e-06, 3.814697265606496e-06]
    two = type_dic[dtype](2.0)
    at_ls = np.array([np.arctan(two / (1 << i)) for i in range(length)], type_dic[dtype])
    # print(at_ls)
    offset = 0
    for j in range(length):
        tangent = (two / (1 << offset))
        if rad >= 0:
            d = 1
        else:
            d = -1
        x_new = x - d * y * tangent
        y_new = y + d * x * tangent
        x = x_new
        y = y_new
        # rad = rad - d * math.atan(pow(2, -(j - 1)))
        rad = rad - d * at_ls[offset]
        if rad < 0:
            offset += 1
        # at_ls.append(math.atan(pow(2, -(j - 1))))
    # print(at_ls)
    # print(x, y)
    # x *= k
    # y *= k
    # print(rad)
    # return y / np.sqrt(x ** 2 + y ** 2), x / np.sqrt(x ** 2 + y ** 2), y / x
    return (sign * y / x).astype(dtype)


def case_fp32():
    dtype = "float32"
    src = np.random.uniform(-1000, 1000, size=(1024, )).astype(dtype)
    # print(np.cos(45), np.cos(np.pi/4))
    # print(f"src: {src}, sin(src): {np.sin(src)}, cos(src): {np.cos(src)}, tan(src): {np.tan(src)}")
    for rad in src:
        rad = np.float32(1e-6)
        print(np.pi)
        res1 = gen_sin_cos(rad, dtype, 68)
        res2 = np.tan(rad).astype(dtype)
        # print(math.fabs(np.sin(src) - si), math.fabs(np.sin(src) - si) < 1e-4)
        # print(math.fabs(np.cos(src) - co), math.fabs(np.cos(src) - co) < 1e-4)
        error = np.abs(np.abs(res1) - np.abs(res2))/np.abs(res2)
        print(np.mod(rad, np.pi))
        print(res1, res2, res1.dtype, res2.dtype)
        assert error < 1e-4, f"input value: {np.mod(rad, np.pi)}, custom tan: {res1}, numpy tan: {res2}, errors: {error}"
        break


def case_fp16():
    dtype = "float16"
    src = type_dic[dtype](np.pi / 4)
    angle = src * 180 / np.pi
    print(angle)
    # print(np.cos(45), np.cos(np.pi/4))
    print(f"src: {src}, sin(src): {np.sin(src)}, cos(src): {np.cos(src)}, tan(src): {np.tan(src)}")
    si, co, ta = gen_sin_cos(src, dtype, 12)
    print(si, co, ta)
    print(math.fabs(np.sin(src) - si), math.fabs(np.sin(src) - si) < 1e-3)
    print(math.fabs(np.cos(src) - co), math.fabs(np.cos(src) - co) < 1e-3)
    print(math.fabs(np.tan(src) - ta), math.fabs(np.tan(src) - ta) < 1e-3)


def display():
    x = np.linspace(-np.pi, np.pi, 256, endpoint=True)
    y = np.tan(x)
    y2 = np.array([gen_sin_cos(x[i], "float32", 16)[2] for i in range(len(x))], np.float32)
    plt.plot(x, y, "b-", lw=2.5, label="tan")
    plt.show()


if __name__ == '__main__':
    case_fp32()
