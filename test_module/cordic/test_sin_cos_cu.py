import sys

import pycuda.autoinit
import pycuda.driver as drv
from pycuda.compiler import SourceModule
import numpy as np
import time


def test_cuda_demo():
    st = """
        __global__ void demo(float *out, float *theta)
        {
            const float RADS[21] =  {1.1071487177940904, 0.7853981633974483, 0.4636476090008061,
                 0.24497866312686414, 0.12435499454676144,
             0.06241880999595735, 0.031239833430268277, 0.015623728620476831, 0.007812341060101111,
             0.0039062301319669718,
             0.0019531225164788188, 0.0009765621895593195, 0.0004882812111948983, 0.00024414062014936177,
             0.00012207031189367021, 6.103515617420877e-05, 3.0517578115526096e-05, 1.5258789061315762e-05,
             7.62939453110197e-06, 3.814697265606496e-06};
            if(threadIdx.x==0) {
                int number = 20;
                float k = 0.6072529350088812564694;
                float x = 1.0;
                float y = 0.0;
                float rad = theta[0];
                int d;
                float x_new;
                float y_new;
                for(int i=0; i < number; i++){
                    if (rad >= 0.0) {
                        d = 1;
                    } else {
                        d = -1;
                    }
                    x_new = x - d * y * (2.0 / (1 << i));
                    y_new = y + d * x * (2.0 / (1 << i));
                    x = x_new;
                    y = y_new;
                    rad = rad - d * RADS[i];
                }
                
                out[0] = y/x;
                out[4] = x;
                out[5] = y;
                x *= k;
                y *= k;
                out[1] = y/(sqrtf(powf(x, 2) + powf(y, 2)));
                out[2] = x/(sqrtf(powf(x, 2) + powf(y, 2)));
                out[3] = rad;
            }
            
        }
    """
    mod = SourceModule(st)
    thread_num = 1024
    block_size = (thread_num, 1, 1)
    grid = (1, 1, 1)
    func = mod.get_function("demo")
    src0_shape = (thread_num,)
    src0 = np.ones(src0_shape, dtype=np.float32) * (np.pi / 4)
    dst = np.zeros(src0_shape, dtype=np.float32)
    print(src0)
    func(drv.Out(dst), drv.In(src0), block=block_size, grid=grid)
    return dst


def test_case1():
    pass


if __name__ == '__main__':
    out_arr = test_cuda_demo()
    print(out_arr[:32])
    # if sys.argv[1] in globals():
    #     globals()[sys.argv[1]]()
    # else:
    #     print(f"params {sys.argv[1]} is invalid")
