xref: /bionic/libm/upstream-freebsd/lib/msun/src/s_tan.c

android-12.0.0_r3
 HistoryAnnotateLine# Scopes# Navigate#Raw Download
  current directory
1  /* @(#)s_tan.c 5.1 93/09/24 */
2  /*
3   * ====================================================
4   * Copyright (C) 1993 by Sun Microsystems, Inc. All rights reserved.
5   *
6   * Developed at SunPro, a Sun Microsystems, Inc. business.
7   * Permission to use, copy, modify, and distribute this
8   * software is freely granted, provided that this notice
9   * is preserved.
10   * ====================================================
11   */
12
13  #include <sys/cdefs.h>
14  __FBSDID("$FreeBSD$");
15
16  /* tan(x)
17   * Return tangent function of x.
18   *
19   * kernel function:
20   *	__kernel_tan		... tangent function on [-pi/4,pi/4]
21   *	__ieee754_rem_pio2	... argument reduction routine
22   *
23   * Method.
24   *      Let S,C and T denote the sin, cos and tan respectively on
25   *	[-PI/4, +PI/4]. Reduce the argument x to y1+y2 = x-k*pi/2
26   *	in [-pi/4 , +pi/4], and let n = k mod 4.
27   *	We have
28   *
29   *          n        sin(x)      cos(x)        tan(x)
30   *     ----------------------------------------------------------
31   *	    0	       S	   C		 T
32   *	    1	       C	  -S		-1/T
33   *	    2	      -S	  -C		 T
34   *	    3	      -C	   S		-1/T
35   *     ----------------------------------------------------------
36   *
37   * Special cases:
38   *      Let trig be any of sin, cos, or tan.
39   *      trig(+-INF)  is NaN, with signals;
40   *      trig(NaN)    is that NaN;
41   *
42   * Accuracy:
43   *	TRIG(x) returns trig(x) nearly rounded
44   */
45
46  #include <float.h>
47
48  #include "math.h"
49  #define INLINE_REM_PIO2
50  #include "math_private.h"
51  #include "e_rem_pio2.c"
52
53  double
54  tan(double x)
55  {
56  	double y[2],z=0.0;
57  	int32_t n, ix;
58
59      /* High word of x. */
60  	GET_HIGH_WORD(ix,x);
61
62      /* |x| ~< pi/4 */
63  	ix &= 0x7fffffff;
64  	if(ix <= 0x3fe921fb) {
65  	    if(ix<0x3e400000)			/* x < 2**-27 */
66  		if((int)x==0) return x;		/* generate inexact */
67  	    return __kernel_tan(x,z,1);
68  	}
69
70      /* tan(Inf or NaN) is NaN */
71  	else if (ix>=0x7ff00000) return x-x;		/* NaN */
72
73      /* argument reduction needed */
74  	else {
75  	    n = __ieee754_rem_pio2(x,y);
76  	    return __kernel_tan(y[0],y[1],1-((n&1)<<1)); /*   1 -- n even
77  							-1 -- n odd */
78  	}
79  }
80
81  #if (LDBL_MANT_DIG == 53)
82  __weak_reference(tan, tanl);
83  #endif
84

Last Index update Wed Jun 14 08:59:42 CST 2023