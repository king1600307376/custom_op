#include <iostream>
#include <cmath>

int main()
{
    double pi = 3.141592653589793;
    float input = pi/2-1e-11-1e-10;
    float out = tan(input);
    std::cout << out << std::endl;
}