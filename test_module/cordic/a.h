xref: /bionic/libm/x86_64/s_tan.S

android-12.0.0_r3
 HistoryAnnotateLine# Scopes# Navigate#Raw Download
  current directory
1 /*
2 Copyright (c) 2014, Intel Corporation
3 All rights reserved.
4
5 Redistribution and use in source and binary forms, with or without
6 modification, are permitted provided that the following conditions are met:
7
8     * Redistributions of source code must retain the above copyright notice,
9     * this list of conditions and the following disclaimer.
10
11     * Redistributions in binary form must reproduce the above copyright notice,
12     * this list of conditions and the following disclaimer in the documentation
13     * and/or other materials provided with the distribution.
14
15     * Neither the name of Intel Corporation nor the names of its contributors
16     * may be used to endorse or promote products derived from this software
17     * without specific prior written permission.
18
19 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
20 ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
21 WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
22 DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
23 ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
24 (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
25 LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
26 ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
27 (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
28 SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
29 */
30
31 /******************************************************************************/
32 //                     ALGORITHM DESCRIPTION
33 //                     ---------------------
34 //
35 // Polynomials coefficients and other constants.
36 //
37 // Note that in this algorithm, there is a different polynomial for
38 // each breakpoint, so there are 32 sets of polynomial coefficients
39 // as well as 32 instances of the other constants.
40 //
41 // The polynomial coefficients and constants are offset from the start
42 // of the main block as follows:
43 //
44 //   0:  c8 | c0
45 //  16:  c9 | c1
46 //  32: c10 | c2
47 //  48: c11 | c3
48 //  64: c12 | c4
49 //  80: c13 | c5
50 //  96: c14 | c6
51 // 112: c15 | c7
52 // 128: T_hi
53 // 136: T_lo
54 // 144: Sigma
55 // 152: T_hl
56 // 160: Tau
57 // 168: Mask
58 // 176: (end of block)
59 //
60 // The total table size is therefore 5632 bytes.
61 //
62 // Note that c0 and c1 are always zero. We could try storing
63 // other constants here, and just loading the low part of the
64 // SIMD register in these cases, after ensuring the high part
65 // is zero.
66 //
67 // The higher terms of the polynomial are computed in the *low*
68 // part of the SIMD register. This is so we can overlap the
69 // multiplication by r^8 and the unpacking of the other part.
70 //
71 // The constants are:
72 // T_hi + T_lo = accurate constant term in power series
73 // Sigma + T_hl = accurate coefficient of r in power series (Sigma=1 bit)
74 // Tau = multiplier for the reciprocal, always -1 or 0
75 //
76 // The basic reconstruction formula using these constants is:
77 //
78 // High = tau * recip_hi + t_hi
79 // Med = (sgn * r + t_hl * r)_hi
80 // Low = (sgn * r + t_hl * r)_lo +
81 //       tau * recip_lo + T_lo + (T_hl + sigma) * c + pol
82 //
83 // where pol = c0 + c1 * r + c2 * r^2 + ... + c15 * r^15
84 //
85 // (c0 = c1 = 0, but using them keeps SIMD regularity)
86 //
87 // We then do a compensated sum High + Med, add the low parts together
88 // and then do the final sum.
89 //
90 // Here recip_hi + recip_lo is an accurate reciprocal of the remainder
91 // modulo pi/2
92 //
93 // Special cases:
94 //  tan(NaN) = quiet NaN, and raise invalid exception
95 //  tan(INF) = NaN and raise invalid exception
96 //  tan(+/-0) = +/-0
97 //
98 /******************************************************************************/
99
100 #include <private/bionic_asm.h>
101 # -- Begin  tan
102 ENTRY(tan)
103 # parameter 1: %xmm0
104 ..B1.1:
105 ..___tag_value_tan.1:
106         pushq     %rbx
107 ..___tag_value_tan.3:
108         subq      $16, %rsp
109 ..___tag_value_tan.5:
110         movsd     %xmm0, 8(%rsp)
111 ..B1.2:
112         pextrw    $3, %xmm0, %eax
113         andl      $32767, %eax
114         subl      $16314, %eax
115         cmpl      $270, %eax
116         ja        .L_2TAG_PACKET_0.0.1
117         movapd    ONEHALF(%rip), %xmm5
118         movapd    MUL16(%rip), %xmm6
119         unpcklpd  %xmm0, %xmm0
120         movapd    sign_mask(%rip), %xmm4
121         andpd     %xmm0, %xmm4
122         movapd    PI32INV(%rip), %xmm1
123         mulpd     %xmm0, %xmm1
124         orps      %xmm4, %xmm5
125         addpd     %xmm5, %xmm1
126         movapd    %xmm1, %xmm7
127         unpckhpd  %xmm7, %xmm7
128         cvttsd2si %xmm7, %edx
129         cvttpd2dq %xmm1, %xmm1
130         cvtdq2pd  %xmm1, %xmm1
131         mulpd     %xmm6, %xmm1
132         movapd    P_1(%rip), %xmm3
133         movq      QQ_2(%rip), %xmm5
134         addq      $469248, %rdx
135         movapd    P_2(%rip), %xmm4
136         mulpd     %xmm1, %xmm3
137         andq      $31, %rdx
138         mulsd     %xmm1, %xmm5
139         movq      %rdx, %rcx
140         mulpd     %xmm1, %xmm4
141         shlq      $1, %rcx
142         subpd     %xmm3, %xmm0
143         mulpd     P_3(%rip), %xmm1
144         addq      %rcx, %rdx
145         shlq      $2, %rcx
146         addq      %rcx, %rdx
147         addsd     %xmm0, %xmm5
148         movapd    %xmm0, %xmm2
149         subpd     %xmm4, %xmm0
150         movq      ONE(%rip), %xmm6
151         shlq      $4, %rdx
152         lea       Ctable(%rip), %rax
153         andpd     MASK_35(%rip), %xmm5
154         movapd    %xmm0, %xmm3
155         addq      %rdx, %rax
156         subpd     %xmm0, %xmm2
157         unpckhpd  %xmm0, %xmm0
158         divsd     %xmm5, %xmm6
159         subpd     %xmm4, %xmm2
160         movapd    16(%rax), %xmm7
161         subsd     %xmm5, %xmm3
162         mulpd     %xmm0, %xmm7
163         subpd     %xmm1, %xmm2
164         movapd    48(%rax), %xmm1
165         mulpd     %xmm0, %xmm1
166         movapd    96(%rax), %xmm4
167         mulpd     %xmm0, %xmm4
168         addsd     %xmm3, %xmm2
169         movapd    %xmm0, %xmm3
170         mulpd     %xmm0, %xmm0
171         addpd     (%rax), %xmm7
172         addpd     32(%rax), %xmm1
173         mulpd     %xmm0, %xmm1
174         addpd     80(%rax), %xmm4
175         addpd     %xmm1, %xmm7
176         movapd    112(%rax), %xmm1
177         mulpd     %xmm0, %xmm1
178         mulpd     %xmm0, %xmm0
179         addpd     %xmm1, %xmm4
180         movapd    64(%rax), %xmm1
181         mulpd     %xmm0, %xmm1
182         addpd     %xmm1, %xmm7
183         movapd    %xmm3, %xmm1
184         mulpd     %xmm0, %xmm3
185         mulsd     %xmm0, %xmm0
186         mulpd     144(%rax), %xmm1
187         mulpd     %xmm3, %xmm4
188         movq      %xmm1, %xmm3
189         addpd     %xmm4, %xmm7
190         movq      %xmm1, %xmm4
191         mulsd     %xmm7, %xmm0
192         unpckhpd  %xmm7, %xmm7
193         addsd     %xmm7, %xmm0
194         unpckhpd  %xmm1, %xmm1
195         addsd     %xmm1, %xmm3
196         subsd     %xmm3, %xmm4
197         addsd     %xmm4, %xmm1
198         movq      %xmm2, %xmm4
199         movq      144(%rax), %xmm7
200         unpckhpd  %xmm2, %xmm2
201         addsd     152(%rax), %xmm7
202         mulsd     %xmm2, %xmm7
203         addsd     136(%rax), %xmm7
204         addsd     %xmm1, %xmm7
205         addsd     %xmm7, %xmm0
206         movq      ONE(%rip), %xmm7
207         mulsd     %xmm6, %xmm4
208         movq      168(%rax), %xmm2
209         andpd     %xmm6, %xmm2
210         mulsd     %xmm2, %xmm5
211         mulsd     160(%rax), %xmm6
212         subsd     %xmm5, %xmm7
213         subsd     128(%rax), %xmm2
214         subsd     %xmm4, %xmm7
215         mulsd     %xmm6, %xmm7
216         movq      %xmm3, %xmm4
217         subsd     %xmm2, %xmm3
218         addsd     %xmm3, %xmm2
219         subsd     %xmm2, %xmm4
220         addsd     %xmm4, %xmm0
221         subsd     %xmm7, %xmm0
222         addsd     %xmm3, %xmm0
223         jmp       ..B1.4
224 .L_2TAG_PACKET_0.0.1:
225         jg        .L_2TAG_PACKET_1.0.1
226         pextrw    $3, %xmm0, %eax
227         movl      %eax, %edx
228         andl      $32752, %eax
229         je        .L_2TAG_PACKET_2.0.1
230         andl      $32767, %edx
231         cmpl      $15904, %edx
232         jb        .L_2TAG_PACKET_3.0.1
233         movq      %xmm0, %xmm2
234         movq      %xmm0, %xmm3
235         movq      Q_11(%rip), %xmm1
236         mulsd     %xmm0, %xmm2
237         mulsd     %xmm2, %xmm3
238         mulsd     %xmm2, %xmm1
239         addsd     Q_9(%rip), %xmm1
240         mulsd     %xmm2, %xmm1
241         addsd     Q_7(%rip), %xmm1
242         mulsd     %xmm2, %xmm1
243         addsd     Q_5(%rip), %xmm1
244         mulsd     %xmm2, %xmm1
245         addsd     Q_3(%rip), %xmm1
246         mulsd     %xmm3, %xmm1
247         addsd     %xmm1, %xmm0
248         jmp       ..B1.4
249 .L_2TAG_PACKET_3.0.1:
250         movq      TWO_POW_55(%rip), %xmm3
251         mulsd     %xmm0, %xmm3
252         addsd     %xmm3, %xmm0
253         mulsd     TWO_POW_M55(%rip), %xmm0
254         jmp       ..B1.4
255 .L_2TAG_PACKET_2.0.1:
256         movq      %xmm0, %xmm1
257         mulsd     %xmm1, %xmm1
258         jmp       ..B1.4
259 .L_2TAG_PACKET_1.0.1:
260         pextrw    $3, %xmm0, %eax
261         andl      $32752, %eax
262         cmpl      $32752, %eax
263         je        .L_2TAG_PACKET_4.0.1
264         pextrw    $3, %xmm0, %ecx
265         andl      $32752, %ecx
266         subl      $16224, %ecx
267         shrl      $7, %ecx
268         andl      $65532, %ecx
269         lea       PI_INV_TABLE(%rip), %r11
270         addq      %r11, %rcx
271         movd      %xmm0, %rax
272         movl      20(%rcx), %r10d
273         movl      24(%rcx), %r8d
274         movl      %eax, %edx
275         shrq      $21, %rax
276         orl       $-2147483648, %eax
277         shrl      $11, %eax
278         movl      %r10d, %r9d
279         imulq     %rdx, %r10
280         imulq     %rax, %r9
281         imulq     %rax, %r8
282         movl      16(%rcx), %esi
283         movl      12(%rcx), %edi
284         movl      %r10d, %r11d
285         shrq      $32, %r10
286         addq      %r10, %r9
287         addq      %r8, %r11
288         movl      %r11d, %r8d
289         shrq      $32, %r11
290         addq      %r11, %r9
291         movl      %esi, %r10d
292         imulq     %rdx, %rsi
293         imulq     %rax, %r10
294         movl      %edi, %r11d
295         imulq     %rdx, %rdi
296         movl      %esi, %ebx
297         shrq      $32, %rsi
298         addq      %rbx, %r9
299         movl      %r9d, %ebx
300         shrq      $32, %r9
301         addq      %rsi, %r10
302         addq      %r9, %r10
303         shlq      $32, %rbx
304         orq       %rbx, %r8
305         imulq     %rax, %r11
306         movl      8(%rcx), %r9d
307         movl      4(%rcx), %esi
308         movl      %edi, %ebx
309         shrq      $32, %rdi
310         addq      %rbx, %r10
311         movl      %r10d, %ebx
312         shrq      $32, %r10
313         addq      %rdi, %r11
314         addq      %r10, %r11
315         movq      %r9, %rdi
316         imulq     %rdx, %r9
317         imulq     %rax, %rdi
318         movl      %r9d, %r10d
319         shrq      $32, %r9
320         addq      %r10, %r11
321         movl      %r11d, %r10d
322         shrq      $32, %r11
323         addq      %r9, %rdi
324         addq      %r11, %rdi
325         movq      %rsi, %r9
326         imulq     %rdx, %rsi
327         imulq     %rax, %r9
328         shlq      $32, %r10
329         orq       %rbx, %r10
330         movl      (%rcx), %eax
331         movl      %esi, %r11d
332         shrq      $32, %rsi
333         addq      %r11, %rdi
334         movl      %edi, %r11d
335         shrq      $32, %rdi
336         addq      %rsi, %r9
337         addq      %rdi, %r9
338         imulq     %rax, %rdx
339         pextrw    $3, %xmm0, %ebx
340         lea       PI_INV_TABLE(%rip), %rdi
341         subq      %rdi, %rcx
342         addl      %ecx, %ecx
343         addl      %ecx, %ecx
344         addl      %ecx, %ecx
345         addl      $19, %ecx
346         movl      $32768, %esi
347         andl      %ebx, %esi
348         shrl      $4, %ebx
349         andl      $2047, %ebx
350         subl      $1023, %ebx
351         subl      %ebx, %ecx
352         addq      %rdx, %r9
353         movl      %ecx, %edx
354         addl      $32, %edx
355         cmpl      $0, %ecx
356         jl        .L_2TAG_PACKET_5.0.1
357         negl      %ecx
358         addl      $29, %ecx
359         shll      %cl, %r9d
360         movl      %r9d, %edi
361         andl      $1073741823, %r9d
362         testl     $536870912, %r9d
363         jne       .L_2TAG_PACKET_6.0.1
364         shrl      %cl, %r9d
365         movl      $0, %ebx
366         shlq      $32, %r9
367         orq       %r11, %r9
368 .L_2TAG_PACKET_7.0.1:
369 .L_2TAG_PACKET_8.0.1:
370         cmpq      $0, %r9
371         je        .L_2TAG_PACKET_9.0.1
372 .L_2TAG_PACKET_10.0.1:
373         bsr       %r9, %r11
374         movl      $29, %ecx
375         subl      %r11d, %ecx
376         jle       .L_2TAG_PACKET_11.0.1
377         shlq      %cl, %r9
378         movq      %r10, %rax
379         shlq      %cl, %r10
380         addl      %ecx, %edx
381         negl      %ecx
382         addl      $64, %ecx
383         shrq      %cl, %rax
384         shrq      %cl, %r8
385         orq       %rax, %r9
386         orq       %r8, %r10
387 .L_2TAG_PACKET_12.0.1:
388         cvtsi2sdq %r9, %xmm0
389         shrq      $1, %r10
390         cvtsi2sdq %r10, %xmm3
391         xorpd     %xmm4, %xmm4
392         shll      $4, %edx
393         negl      %edx
394         addl      $16368, %edx
395         orl       %esi, %edx
396         xorl      %ebx, %edx
397         pinsrw    $3, %edx, %xmm4
398         movq      PI_4(%rip), %xmm2
399         movq      8+PI_4(%rip), %xmm7
400         xorpd     %xmm5, %xmm5
401         subl      $1008, %edx
402         pinsrw    $3, %edx, %xmm5
403         mulsd     %xmm4, %xmm0
404         shll      $16, %esi
405         sarl      $31, %esi
406         mulsd     %xmm5, %xmm3
407         movq      %xmm0, %xmm1
408         mulsd     %xmm2, %xmm0
409         shrl      $30, %edi
410         addsd     %xmm3, %xmm1
411         mulsd     %xmm2, %xmm3
412         addl      %esi, %edi
413         xorl      %esi, %edi
414         mulsd     %xmm1, %xmm7
415         movl      %edi, %eax
416         addsd     %xmm3, %xmm7
417         movq      %xmm0, %xmm2
418         addsd     %xmm7, %xmm0
419         subsd     %xmm0, %xmm2
420         addsd     %xmm2, %xmm7
421         movapd    PI32INV(%rip), %xmm1
422         movddup   %xmm0, %xmm0
423         movapd    sign_mask(%rip), %xmm4
424         andpd     %xmm0, %xmm4
425         mulpd     %xmm0, %xmm1
426         movddup   %xmm7, %xmm7
427         movapd    ONEHALF(%rip), %xmm5
428         movapd    MUL16(%rip), %xmm6
429         orps      %xmm4, %xmm5
430         addpd     %xmm5, %xmm1
431         movapd    %xmm1, %xmm5
432         unpckhpd  %xmm5, %xmm5
433         cvttsd2si %xmm5, %edx
434         cvttpd2dq %xmm1, %xmm1
435         cvtdq2pd  %xmm1, %xmm1
436         mulpd     %xmm6, %xmm1
437         movapd    P_1(%rip), %xmm3
438         movq      QQ_2(%rip), %xmm5
439         shll      $4, %eax
440         addl      $469248, %edx
441         movapd    P_2(%rip), %xmm4
442         mulpd     %xmm1, %xmm3
443         addl      %eax, %edx
444         andl      $31, %edx
445         mulsd     %xmm1, %xmm5
446         movl      %edx, %ecx
447         mulpd     %xmm1, %xmm4
448         shll      $1, %ecx
449         subpd     %xmm3, %xmm0
450         mulpd     P_3(%rip), %xmm1
451         addl      %ecx, %edx
452         shll      $2, %ecx
453         addl      %ecx, %edx
454         addsd     %xmm0, %xmm5
455         movapd    %xmm0, %xmm2
456         subpd     %xmm4, %xmm0
457         movq      ONE(%rip), %xmm6
458         shll      $4, %edx
459         lea       Ctable(%rip), %rax
460         andpd     MASK_35(%rip), %xmm5
461         movapd    %xmm0, %xmm3
462         addq      %rdx, %rax
463         subpd     %xmm0, %xmm2
464         unpckhpd  %xmm0, %xmm0
465         divsd     %xmm5, %xmm6
466         subpd     %xmm4, %xmm2
467         subsd     %xmm5, %xmm3
468         subpd     %xmm1, %xmm2
469         movapd    48(%rax), %xmm1
470         addpd     %xmm7, %xmm2
471         movapd    16(%rax), %xmm7
472         mulpd     %xmm0, %xmm7
473         movapd    96(%rax), %xmm4
474         mulpd     %xmm0, %xmm1
475         mulpd     %xmm0, %xmm4
476         addsd     %xmm3, %xmm2
477         movapd    %xmm0, %xmm3
478         mulpd     %xmm0, %xmm0
479         addpd     (%rax), %xmm7
480         addpd     32(%rax), %xmm1
481         mulpd     %xmm0, %xmm1
482         addpd     80(%rax), %xmm4
483         addpd     %xmm1, %xmm7
484         movapd    112(%rax), %xmm1
485         mulpd     %xmm0, %xmm1
486         mulpd     %xmm0, %xmm0
487         addpd     %xmm1, %xmm4
488         movapd    64(%rax), %xmm1
489         mulpd     %xmm0, %xmm1
490         addpd     %xmm1, %xmm7
491         movapd    %xmm3, %xmm1
492         mulpd     %xmm0, %xmm3
493         mulsd     %xmm0, %xmm0
494         mulpd     144(%rax), %xmm1
495         mulpd     %xmm3, %xmm4
496         movq      %xmm1, %xmm3
497         addpd     %xmm4, %xmm7
498         movq      %xmm1, %xmm4
499         mulsd     %xmm7, %xmm0
500         unpckhpd  %xmm7, %xmm7
501         addsd     %xmm7, %xmm0
502         unpckhpd  %xmm1, %xmm1
503         addsd     %xmm1, %xmm3
504         subsd     %xmm3, %xmm4
505         addsd     %xmm4, %xmm1
506         movq      %xmm2, %xmm4
507         movq      144(%rax), %xmm7
508         unpckhpd  %xmm2, %xmm2
509         addsd     152(%rax), %xmm7
510         mulsd     %xmm2, %xmm7
511         addsd     136(%rax), %xmm7
512         addsd     %xmm1, %xmm7
513         addsd     %xmm7, %xmm0
514         movq      ONE(%rip), %xmm7
515         mulsd     %xmm6, %xmm4
516         movq      168(%rax), %xmm2
517         andpd     %xmm6, %xmm2
518         mulsd     %xmm2, %xmm5
519         mulsd     160(%rax), %xmm6
520         subsd     %xmm5, %xmm7
521         subsd     128(%rax), %xmm2
522         subsd     %xmm4, %xmm7
523         mulsd     %xmm6, %xmm7
524         movq      %xmm3, %xmm4
525         subsd     %xmm2, %xmm3
526         addsd     %xmm3, %xmm2
527         subsd     %xmm2, %xmm4
528         addsd     %xmm4, %xmm0
529         subsd     %xmm7, %xmm0
530         addsd     %xmm3, %xmm0
531         jmp       ..B1.4
532 .L_2TAG_PACKET_9.0.1:
533         addl      $64, %edx
534         movq      %r10, %r9
535         movq      %r8, %r10
536         movq      $0, %r8
537         cmpq      $0, %r9
538         jne       .L_2TAG_PACKET_10.0.1
539         addl      $64, %edx
540         movq      %r10, %r9
541         movq      %r8, %r10
542         cmpq      $0, %r9
543         jne       .L_2TAG_PACKET_10.0.1
544         jmp       .L_2TAG_PACKET_12.0.1
545 .L_2TAG_PACKET_11.0.1:
546         je        .L_2TAG_PACKET_12.0.1
547         negl      %ecx
548         shrq      %cl, %r10
549         movq      %r9, %rax
550         shrq      %cl, %r9
551         subl      %ecx, %edx
552         negl      %ecx
553         addl      $64, %ecx
554         shlq      %cl, %rax
555         orq       %rax, %r10
556         jmp       .L_2TAG_PACKET_12.0.1
557 .L_2TAG_PACKET_5.0.1:
558         notl      %ecx
559         shlq      $32, %r9
560         orq       %r11, %r9
561         shlq      %cl, %r9
562         movq      %r9, %rdi
563         testl     $-2147483648, %r9d
564         jne       .L_2TAG_PACKET_13.0.1
565         shrl      %cl, %r9d
566         movl      $0, %ebx
567         shrq      $2, %rdi
568         jmp       .L_2TAG_PACKET_8.0.1
569 .L_2TAG_PACKET_6.0.1:
570         shrl      %cl, %r9d
571         movl      $1073741824, %ebx
572         shrl      %cl, %ebx
573         shlq      $32, %r9
574         orq       %r11, %r9
575         shlq      $32, %rbx
576         addl      $1073741824, %edi
577         movq      $0, %rcx
578         movq      $0, %r11
579         subq      %r8, %rcx
580         sbbq      %r10, %r11
581         sbbq      %r9, %rbx
582         movq      %rcx, %r8
583         movq      %r11, %r10
584         movq      %rbx, %r9
585         movl      $32768, %ebx
586         jmp       .L_2TAG_PACKET_7.0.1
587 .L_2TAG_PACKET_13.0.1:
588         shrl      %cl, %r9d
589         movq      $0x100000000, %rbx
590         shrq      %cl, %rbx
591         movq      $0, %rcx
592         movq      $0, %r11
593         subq      %r8, %rcx
594         sbbq      %r10, %r11
595         sbbq      %r9, %rbx
596         movq      %rcx, %r8
597         movq      %r11, %r10
598         movq      %rbx, %r9
599         movl      $32768, %ebx
600         shrq      $2, %rdi
601         addl      $1073741824, %edi
602         jmp       .L_2TAG_PACKET_8.0.1
603 .L_2TAG_PACKET_4.0.1:
604         movq      8(%rsp), %xmm0
605         mulsd     NEG_ZERO(%rip), %xmm0
606         movq      %xmm0, (%rsp)
607 .L_2TAG_PACKET_14.0.1:
608 ..B1.4:
609         addq      $16, %rsp
610 ..___tag_value_tan.6:
611         popq      %rbx
612 ..___tag_value_tan.8:
613         ret
614 ..___tag_value_tan.9:
615 END(tan)
616 # -- End  tan
617 	.section .rodata, "a"
618 	.align 16
619 	.align 16
620 ONEHALF:
621 	.long	0
622 	.long	1071644672
623 	.long	0
624 	.long	1071644672
625 	.type	ONEHALF,@object
626 	.size	ONEHALF,16
627 	.align 16
628 MUL16:
629 	.long	0
630 	.long	1076887552
631 	.long	0
632 	.long	1072693248
633 	.type	MUL16,@object
634 	.size	MUL16,16
635 	.align 16
636 sign_mask:
637 	.long	0
638 	.long	2147483648
639 	.long	0
640 	.long	2147483648
641 	.type	sign_mask,@object
642 	.size	sign_mask,16
643 	.align 16
644 PI32INV:
645 	.long	1841940611
646 	.long	1071931184
647 	.long	1841940611
648 	.long	1076125488
649 	.type	PI32INV,@object
650 	.size	PI32INV,16
651 	.align 16
652 P_1:
653 	.long	1413758976
654 	.long	1069097467
655 	.long	1413742592
656 	.long	1069097467
657 	.type	P_1,@object
658 	.size	P_1,16
659 	.align 16
660 P_2:
661 	.long	1734819840
662 	.long	3174229945
663 	.long	1280049152
664 	.long	1028033571
665 	.type	P_2,@object
666 	.size	P_2,16
667 	.align 16
668 P_3:
669 	.long	923219018
670 	.long	984130272
671 	.long	57701189
672 	.long	988383790
673 	.type	P_3,@object
674 	.size	P_3,16
675 	.align 16
676 Ctable:
677 	.long	0
678 	.long	0
679 	.long	0
680 	.long	0
681 	.long	2284589306
682 	.long	1066820852
683 	.long	0
684 	.long	0
685 	.long	0
686 	.long	0
687 	.long	0
688 	.long	0
689 	.long	1441186365
690 	.long	1065494243
691 	.long	1431655765
692 	.long	1070945621
693 	.long	0
694 	.long	0
695 	.long	0
696 	.long	0
697 	.long	236289504
698 	.long	1064135997
699 	.long	286331153
700 	.long	1069617425
701 	.long	0
702 	.long	0
703 	.long	0
704 	.long	0
705 	.long	1160476131
706 	.long	1062722102
707 	.long	463583772
708 	.long	1068212666
709 	.long	0
710 	.long	0
711 	.long	0
712 	.long	0
713 	.long	0
714 	.long	1072693248
715 	.long	0
716 	.long	0
717 	.long	0
718 	.long	0
719 	.long	0
720 	.long	0
721 	.long	1313038235
722 	.long	1066745731
723 	.long	0
724 	.long	0
725 	.long	1013878342
726 	.long	1067152618
727 	.long	0
728 	.long	0
729 	.long	3663426833
730 	.long	1065725283
731 	.long	3693284251
732 	.long	1069118808
733 	.long	650852232
734 	.long	1065882376
735 	.long	1996245381
736 	.long	1071000265
737 	.long	2008746170
738 	.long	1064664197
739 	.long	3055842593
740 	.long	1068578846
741 	.long	1495406348
742 	.long	1064652437
743 	.long	2269530157
744 	.long	1069711235
745 	.long	285563696
746 	.long	1063576465
747 	.long	1046897440
748 	.long	1067705865
749 	.long	233429731
750 	.long	1063453151
751 	.long	522045958
752 	.long	1068476590
753 	.long	2354785698
754 	.long	1069102779
755 	.long	1317599141
756 	.long	1012432133
757 	.long	0
758 	.long	1072693248
759 	.long	2828230105
760 	.long	1065606626
761 	.long	0
762 	.long	0
763 	.long	0
764 	.long	0
765 	.long	1512545955
766 	.long	1068119047
767 	.long	0
768 	.long	0
769 	.long	1127048698
770 	.long	1067909459
771 	.long	0
772 	.long	0
773 	.long	2300200450
774 	.long	1067254767
775 	.long	3593250296
776 	.long	1070233561
777 	.long	3009365544
778 	.long	1066902117
779 	.long	1127373050
780 	.long	1071173457
781 	.long	3046103305
782 	.long	1066371299
783 	.long	24583402
784 	.long	1069723988
785 	.long	4082511758
786 	.long	1065914199
787 	.long	3223889699
788 	.long	1070020367
789 	.long	548927984
790 	.long	1065415756
791 	.long	558065897
792 	.long	1068949418
793 	.long	680073315
794 	.long	1064940726
795 	.long	388873200
796 	.long	1068944270
797 	.long	3763679576
798 	.long	1070167541
799 	.long	1497360404
800 	.long	1009710547
801 	.long	0
802 	.long	1072693248
803 	.long	64931152
804 	.long	1067729411
805 	.long	0
806 	.long	0
807 	.long	0
808 	.long	0
809 	.long	2467582782
810 	.long	1069256389
811 	.long	0
812 	.long	0
813 	.long	162150096
814 	.long	1068946420
815 	.long	0
816 	.long	0
817 	.long	3702794237
818 	.long	1068579152
819 	.long	3631919291
820 	.long	1070936926
821 	.long	3456821413
822 	.long	1068217218
823 	.long	2031366438
824 	.long	1071495745
825 	.long	1596664020
826 	.long	1067799281
827 	.long	1509038701
828 	.long	1070601643
829 	.long	583171477
830 	.long	1067510148
831 	.long	3785344682
832 	.long	1070618476
833 	.long	2402036048
834 	.long	1067075736
835 	.long	3233018412
836 	.long	1069913186
837 	.long	411280568
838 	.long	1066710556
839 	.long	1065584192
840 	.long	1069747896
841 	.long	895247324
842 	.long	1070819848
843 	.long	500078909
844 	.long	3161288781
845 	.long	0
846 	.long	1072693248
847 	.long	729983843
848 	.long	1068994194
849 	.long	0
850 	.long	0
851 	.long	0
852 	.long	0
853 	.long	1458794562
854 	.long	1070398550
855 	.long	0
856 	.long	0
857 	.long	2857777489
858 	.long	1070137637
859 	.long	0
860 	.long	0
861 	.long	1024359517
862 	.long	1069876531
863 	.long	2616040238
864 	.long	1071582937
865 	.long	1609024636
866 	.long	1069675088
867 	.long	2529240549
868 	.long	1071836633
869 	.long	1510128600
870 	.long	1069440113
871 	.long	2251697184
872 	.long	1071253687
873 	.long	1262761453
874 	.long	1069142850
875 	.long	1263091857
876 	.long	1071190461
877 	.long	3043383486
878 	.long	1068885191
879 	.long	2476932470
880 	.long	1070842002
881 	.long	3659995028
882 	.long	1068669200
883 	.long	855891755
884 	.long	1070696894
885 	.long	2583490354
886 	.long	1071284857
887 	.long	3062633575
888 	.long	1014008623
889 	.long	0
890 	.long	1072693248
891 	.long	2550940471
892 	.long	1069938201
893 	.long	0
894 	.long	0
895 	.long	0
896 	.long	0
897 	.long	3422807297
898 	.long	1071640847
899 	.long	0
900 	.long	0
901 	.long	1151658053
902 	.long	1071494715
903 	.long	0
904 	.long	0
905 	.long	929607071
906 	.long	1071346340
907 	.long	1037049034
908 	.long	1072037305
909 	.long	2786928657
910 	.long	1071215282
911 	.long	1447406859
912 	.long	1072265209
913 	.long	3490952107
914 	.long	1071090851
915 	.long	3205232916
916 	.long	1071968658
917 	.long	1297344304
918 	.long	1070977120
919 	.long	1066110976
920 	.long	1071946035
921 	.long	3803721480
922 	.long	1070871082
923 	.long	1496754229
924 	.long	1071807201
925 	.long	2982550683
926 	.long	1070773243
927 	.long	4014441989
928 	.long	1071736222
929 	.long	419968236
930 	.long	1071717047
931 	.long	3451266538
932 	.long	3163444811
933 	.long	0
934 	.long	1072693248
935 	.long	2960267235
936 	.long	1070745841
937 	.long	0
938 	.long	0
939 	.long	0
940 	.long	0
941 	.long	724322768
942 	.long	1072881308
943 	.long	0
944 	.long	0
945 	.long	643153048
946 	.long	1072905816
947 	.long	0
948 	.long	0
949 	.long	4285079458
950 	.long	1072928558
951 	.long	3912524733
952 	.long	1072622983
953 	.long	118362272
954 	.long	1072952754
955 	.long	4107767972
956 	.long	1072827408
957 	.long	2689502883
958 	.long	1072976922
959 	.long	946523347
960 	.long	1072772766
961 	.long	573204189
962 	.long	1073001761
963 	.long	581531518
964 	.long	1072826391
965 	.long	1386236526
966 	.long	1073026959
967 	.long	3718905905
968 	.long	1072832823
969 	.long	1145558140
970 	.long	1073052673
971 	.long	513572637
972 	.long	1072861969
973 	.long	716700048
974 	.long	1071997368
975 	.long	547126769
976 	.long	1015523525
977 	.long	0
978 	.long	1072693248
979 	.long	1097907398
980 	.long	1071420120
981 	.long	0
982 	.long	0
983 	.long	0
984 	.long	0
985 	.long	3349892442
986 	.long	1074290212
987 	.long	0
988 	.long	0
989 	.long	3913197405
990 	.long	1074501181
991 	.long	0
992 	.long	0
993 	.long	2494034522
994 	.long	1074739170
995 	.long	1264738763
996 	.long	1073084804
997 	.long	1520293906
998 	.long	1074899632
999 	.long	1958936600
1000 	.long	1073411493
1001 	.long	2133649635
1002 	.long	1075052171
1003 	.long	4270740730
1004 	.long	1073574708
1005 	.long	1728930189
1006 	.long	1075224844
1007 	.long	1303998552
1008 	.long	1073799186
1009 	.long	618611933
1010 	.long	1075420255
1011 	.long	1769828046
1012 	.long	1073938542
1013 	.long	2200537986
1014 	.long	1075641421
1015 	.long	433361110
1016 	.long	1074105369
1017 	.long	719595600
1018 	.long	1072317184
1019 	.long	294527206
1020 	.long	3162140088
1021 	.long	0
1022 	.long	1073741824
1023 	.long	3811788216
1024 	.long	3218400550
1025 	.long	0
1026 	.long	0
1027 	.long	0
1028 	.long	0
1029 	.long	1704352102
1030 	.long	1075943001
1031 	.long	0
1032 	.long	0
1033 	.long	2284589306
1034 	.long	1076258036
1035 	.long	0
1036 	.long	0
1037 	.long	2211264291
1038 	.long	1076659010
1039 	.long	0
1040 	.long	1073741824
1041 	.long	1441186365
1042 	.long	1077028579
1043 	.long	1431655765
1044 	.long	1074091349
1045 	.long	876943673
1046 	.long	1077353622
1047 	.long	2863311531
1048 	.long	1074440874
1049 	.long	236289504
1050 	.long	1077767485
1051 	.long	286331153
1052 	.long	1074860305
1053 	.long	2805473311
1054 	.long	1078115278
1055 	.long	95443718
1056 	.long	1075163227
1057 	.long	1160476131
1058 	.long	1078450742
1059 	.long	463583772
1060 	.long	1075552698
1061 	.long	0
1062 	.long	1072693248
1063 	.long	0
1064 	.long	0
1065 	.long	0
1066 	.long	1073741824
1067 	.long	0
1068 	.long	0
1069 	.long	0
1070 	.long	0
1071 	.long	0
1072 	.long	0
1073 	.long	1330165971
1074 	.long	3207850745
1075 	.long	0
1076 	.long	0
1077 	.long	217536623
1078 	.long	1059109098
1079 	.long	0
1080 	.long	0
1081 	.long	3492120849
1082 	.long	3205151475
1083 	.long	602185705
1084 	.long	3215678092
1085 	.long	760422958
1086 	.long	1056312597
1087 	.long	555127889
1088 	.long	1067545266
1089 	.long	3139784124
1090 	.long	3202470837
1091 	.long	3690544014
1092 	.long	3213150171
1093 	.long	95707915
1094 	.long	1053635428
1095 	.long	4003114407
1096 	.long	1064581412
1097 	.long	2034926231
1098 	.long	3199711161
1099 	.long	3759536023
1100 	.long	3210559989
1101 	.long	3826928214
1102 	.long	1050893819
1103 	.long	3837960785
1104 	.long	1061790379
1105 	.long	1526325248
1106 	.long	3217967566
1107 	.long	2356426521
1108 	.long	1025423456
1109 	.long	0
1110 	.long	0
1111 	.long	457728975
1112 	.long	1071088276
1113 	.long	0
1114 	.long	1072693248
1115 	.long	0
1116 	.long	4294967288
1117 	.long	1398462608
1118 	.long	3207303968
1119 	.long	0
1120 	.long	0
1121 	.long	26205983
1122 	.long	1058461213
1123 	.long	0
1124 	.long	0
1125 	.long	56226238
1126 	.long	3204528612
1127 	.long	2754706541
1128 	.long	3215359511
1129 	.long	2187799823
1130 	.long	1055634437
1131 	.long	790323742
1132 	.long	1067402587
1133 	.long	1372385848
1134 	.long	3201651479
1135 	.long	4097292716
1136 	.long	3212856302
1137 	.long	3348210357
1138 	.long	1052830099
1139 	.long	2442796466
1140 	.long	1064337602
1141 	.long	862608142
1142 	.long	3198830754
1143 	.long	170296152
1144 	.long	3210060867
1145 	.long	3755571428
1146 	.long	1049933343
1147 	.long	3614866008
1148 	.long	1061361670
1149 	.long	719978496
1150 	.long	3217669096
1151 	.long	1998842465
1152 	.long	3174703977
1153 	.long	0
1154 	.long	0
1155 	.long	3749156607
1156 	.long	1071048258
1157 	.long	0
1158 	.long	1072693248
1159 	.long	0
1160 	.long	4294967288
1161 	.long	3120498638
1162 	.long	3206749304
1163 	.long	0
1164 	.long	0
1165 	.long	2773578114
1166 	.long	1058009312
1167 	.long	0
1168 	.long	0
1169 	.long	2030783676
1170 	.long	3203817873
1171 	.long	2223654598
1172 	.long	3215071936
1173 	.long	2976134650
1174 	.long	1054987244
1175 	.long	706390066
1176 	.long	1067217386
1177 	.long	4258437615
1178 	.long	3200900378
1179 	.long	1066252975
1180 	.long	3212391267
1181 	.long	815777514
1182 	.long	1051989462
1183 	.long	3202745457
1184 	.long	1064010682
1185 	.long	2493556375
1186 	.long	3198004753
1187 	.long	1046243251
1188 	.long	3209678971
1189 	.long	2593078846
1190 	.long	1049017717
1191 	.long	2763962276
1192 	.long	1060970161
1193 	.long	701480960
1194 	.long	3217377742
1195 	.long	3205862232
1196 	.long	3174660915
1197 	.long	0
1198 	.long	0
1199 	.long	2267016812
1200 	.long	1071015664
1201 	.long	0
1202 	.long	1072693248
1203 	.long	0
1204 	.long	4294967288
1205 	.long	2107155798
1206 	.long	3206166872
1207 	.long	0
1208 	.long	0
1209 	.long	2642992129
1210 	.long	1057424578
1211 	.long	0
1212 	.long	0
1213 	.long	1936992811
1214 	.long	3203204426
1215 	.long	1485063559
1216 	.long	3214682643
1217 	.long	1432914553
1218 	.long	1054319398
1219 	.long	3996381654
1220 	.long	1067075828
1221 	.long	2833029256
1222 	.long	3200223545
1223 	.long	2866066872
1224 	.long	3211982662
1225 	.long	2432888737
1226 	.long	1051234178
1227 	.long	3669764559
1228 	.long	1063748136
1229 	.long	2458496952
1230 	.long	3197170774
1231 	.long	1948234989
1232 	.long	3209098147
1233 	.long	2843698787
1234 	.long	1048163519
1235 	.long	3398041407
1236 	.long	1060559728
1237 	.long	2829230080
1238 	.long	3217092115
1239 	.long	1034046433
1240 	.long	3174271903
1241 	.long	0
1242 	.long	0
1243 	.long	298675305
1244 	.long	1070989821
1245 	.long	0
1246 	.long	1072693248
1247 	.long	0
1248 	.long	4294967288
1249 	.long	437603223
1250 	.long	3205589761
1251 	.long	0
1252 	.long	0
1253 	.long	759330352
1254 	.long	1057048511
1255 	.long	0
1256 	.long	0
1257 	.long	3107463368
1258 	.long	3202507988
1259 	.long	3144465176
1260 	.long	3214191500
1261 	.long	2290961810
1262 	.long	1053841035
1263 	.long	1618153340
1264 	.long	1066971547
1265 	.long	3836869393
1266 	.long	3199400272
1267 	.long	584032116
1268 	.long	3211469261
1269 	.long	1245704358
1270 	.long	1050626462
1271 	.long	4247487438
1272 	.long	1063561943
1273 	.long	1669034927
1274 	.long	3196274812
1275 	.long	3844233498
1276 	.long	3208626322
1277 	.long	2706958524
1278 	.long	1047411374
1279 	.long	3857199098
1280 	.long	1060281647
1281 	.long	3593904128
1282 	.long	3216590719
1283 	.long	3267547836
1284 	.long	3172163321
1285 	.long	0
1286 	.long	0
1287 	.long	4076712227
1288 	.long	1070970214
1289 	.long	0
1290 	.long	1072693248
1291 	.long	0
1292 	.long	4294967288
1293 	.long	3290090340
1294 	.long	3204793485
1295 	.long	0
1296 	.long	0
1297 	.long	3685760367
1298 	.long	1056668370
1299 	.long	0
1300 	.long	0
1301 	.long	2655163949
1302 	.long	3201674917
1303 	.long	628750575
1304 	.long	3213566872
1305 	.long	680140505
1306 	.long	1053299777
1307 	.long	2954464709
1308 	.long	1066900026
1309 	.long	803201619
1310 	.long	3198516435
1311 	.long	1466315631
1312 	.long	3210837162
1313 	.long	1611220163
1314 	.long	1049972438
1315 	.long	2766187256
1316 	.long	1063437894
1317 	.long	1804579484
1318 	.long	3195331491
1319 	.long	3695969289
1320 	.long	3207854418
1321 	.long	2617238373
1322 	.long	1046675948
1323 	.long	3095830084
1324 	.long	1060095334
1325 	.long	3789570048
1326 	.long	3216034914
1327 	.long	23826559
1328 	.long	3172048060
1329 	.long	0
1330 	.long	0
1331 	.long	3870939386
1332 	.long	1070956467
1333 	.long	0
1334 	.long	1072693248
1335 	.long	0
1336 	.long	4294967288
1337 	.long	1571758758
1338 	.long	3203672535
1339 	.long	0
1340 	.long	0
1341 	.long	113026373
1342 	.long	1056416381
1343 	.long	0
1344 	.long	0
1345 	.long	1913766298
1346 	.long	3200523326
1347 	.long	2507068734
1348 	.long	3212502004
1349 	.long	4000648818
1350 	.long	1053003803
1351 	.long	2446607349
1352 	.long	1066858259
1353 	.long	912662124
1354 	.long	3197333001
1355 	.long	1349489537
1356 	.long	3209765608
1357 	.long	3412972607
1358 	.long	1049641401
1359 	.long	1721283327
1360 	.long	1063366855
1361 	.long	1466691883
1362 	.long	3194116746
1363 	.long	3852528092
1364 	.long	3206760861
1365 	.long	285443293
1366 	.long	1046158380
1367 	.long	1758739894
1368 	.long	1059895449
1369 	.long	1858781184
1370 	.long	3214984212
1371 	.long	3447575948
1372 	.long	1024675855
1373 	.long	0
1374 	.long	0
1375 	.long	2242038011
1376 	.long	1070948320
1377 	.long	0
1378 	.long	1072693248
1379 	.long	0
1380 	.long	4294967288
1381 	.long	0
1382 	.long	0
1383 	.long	0
1384 	.long	0
1385 	.long	737611454
1386 	.long	1056336527
1387 	.long	0
1388 	.long	0
1389 	.long	0
1390 	.long	0
1391 	.long	0
1392 	.long	0
1393 	.long	3594790527
1394 	.long	1052911621
1395 	.long	381774871
1396 	.long	1066844524
1397 	.long	0
1398 	.long	0
1399 	.long	0
1400 	.long	0
1401 	.long	3303051618
1402 	.long	1049456050
1403 	.long	3154187623
1404 	.long	1063343722
1405 	.long	0
1406 	.long	0
1407 	.long	0
1408 	.long	0
1409 	.long	528061788
1410 	.long	1045944910
1411 	.long	2469719819
1412 	.long	1059831159
1413 	.long	0
1414 	.long	0
1415 	.long	0
1416 	.long	0
1417 	.long	0
1418 	.long	0
1419 	.long	1431655765
1420 	.long	1070945621
1421 	.long	0
1422 	.long	1072693248
1423 	.long	0
1424 	.long	4294967288
1425 	.long	1571758758
1426 	.long	1056188887
1427 	.long	0
1428 	.long	0
1429 	.long	113026373
1430 	.long	1056416381
1431 	.long	0
1432 	.long	0
1433 	.long	1913766298
1434 	.long	1053039678
1435 	.long	2507068734
1436 	.long	1065018356
1437 	.long	4000648818
1438 	.long	1053003803
1439 	.long	2446607349
1440 	.long	1066858259
1441 	.long	912662124
1442 	.long	1049849353
1443 	.long	1349489537
1444 	.long	1062281960
1445 	.long	3412972607
1446 	.long	1049641401
1447 	.long	1721283327
1448 	.long	1063366855
1449 	.long	1466691883
1450 	.long	1046633098
1451 	.long	3852528092
1452 	.long	1059277213
1453 	.long	285443293
1454 	.long	1046158380
1455 	.long	1758739894
1456 	.long	1059895449
1457 	.long	1858781184
1458 	.long	1067500564
1459 	.long	3447575948
1460 	.long	3172159503
1461 	.long	0
1462 	.long	0
1463 	.long	2242038011
1464 	.long	1070948320
1465 	.long	0
1466 	.long	1072693248
1467 	.long	0
1468 	.long	4294967288
1469 	.long	3290090340
1470 	.long	1057309837
1471 	.long	0
1472 	.long	0
1473 	.long	3685760367
1474 	.long	1056668370
1475 	.long	0
1476 	.long	0
1477 	.long	2655163949
1478 	.long	1054191269
1479 	.long	628750575
1480 	.long	1066083224
1481 	.long	680140505
1482 	.long	1053299777
1483 	.long	2954464709
1484 	.long	1066900026
1485 	.long	803201619
1486 	.long	1051032787
1487 	.long	1466315631
1488 	.long	1063353514
1489 	.long	1611220163
1490 	.long	1049972438
1491 	.long	2766187256
1492 	.long	1063437894
1493 	.long	1804579484
1494 	.long	1047847843
1495 	.long	3695969289
1496 	.long	1060370770
1497 	.long	2617238373
1498 	.long	1046675948
1499 	.long	3095830084
1500 	.long	1060095334
1501 	.long	3789570048
1502 	.long	1068551266
1503 	.long	23826559
1504 	.long	1024564412
1505 	.long	0
1506 	.long	0
1507 	.long	3870939386
1508 	.long	1070956467
1509 	.long	0
1510 	.long	1072693248
1511 	.long	0
1512 	.long	4294967288
1513 	.long	437603223
1514 	.long	1058106113
1515 	.long	0
1516 	.long	0
1517 	.long	759330352
1518 	.long	1057048511
1519 	.long	0
1520 	.long	0
1521 	.long	3107463368
1522 	.long	1055024340
1523 	.long	3144465176
1524 	.long	1066707852
1525 	.long	2290961810
1526 	.long	1053841035
1527 	.long	1618153340
1528 	.long	1066971547
1529 	.long	3836869393
1530 	.long	1051916624
1531 	.long	584032116
1532 	.long	1063985613
1533 	.long	1245704358
1534 	.long	1050626462
1535 	.long	4247487438
1536 	.long	1063561943
1537 	.long	1669034927
1538 	.long	1048791164
1539 	.long	3844233498
1540 	.long	1061142674
1541 	.long	2706958524
1542 	.long	1047411374
1543 	.long	3857199098
1544 	.long	1060281647
1545 	.long	3593904128
1546 	.long	1069107071
1547 	.long	3267547836
1548 	.long	1024679673
1549 	.long	0
1550 	.long	0
1551 	.long	4076712227
1552 	.long	1070970214
1553 	.long	0
1554 	.long	1072693248
1555 	.long	0
1556 	.long	4294967288
1557 	.long	2107155798
1558 	.long	1058683224
1559 	.long	0
1560 	.long	0
1561 	.long	2642992129
1562 	.long	1057424578
1563 	.long	0
1564 	.long	0
1565 	.long	1936992811
1566 	.long	1055720778
1567 	.long	1485063559
1568 	.long	1067198995
1569 	.long	1432914553
1570 	.long	1054319398
1571 	.long	3996381654
1572 	.long	1067075828
1573 	.long	2833029256
1574 	.long	1052739897
1575 	.long	2866066872
1576 	.long	1064499014
1577 	.long	2432888737
1578 	.long	1051234178
1579 	.long	3669764559
1580 	.long	1063748136
1581 	.long	2458496952
1582 	.long	1049687126
1583 	.long	1948234989
1584 	.long	1061614499
1585 	.long	2843698787
1586 	.long	1048163519
1587 	.long	3398041407
1588 	.long	1060559728
1589 	.long	2829230080
1590 	.long	1069608467
1591 	.long	1034046433
1592 	.long	1026788255
1593 	.long	0
1594 	.long	0
1595 	.long	298675305
1596 	.long	1070989821
1597 	.long	0
1598 	.long	1072693248
1599 	.long	0
1600 	.long	4294967288
1601 	.long	3120498638
1602 	.long	1059265656
1603 	.long	0
1604 	.long	0
1605 	.long	2773578114
1606 	.long	1058009312
1607 	.long	0
1608 	.long	0
1609 	.long	2030783676
1610 	.long	1056334225
1611 	.long	2223654598
1612 	.long	1067588288
1613 	.long	2976134650
1614 	.long	1054987244
1615 	.long	706390066
1616 	.long	1067217386
1617 	.long	4258437615
1618 	.long	1053416730
1619 	.long	1066252975
1620 	.long	1064907619
1621 	.long	815777514
1622 	.long	1051989462
1623 	.long	3202745457
1624 	.long	1064010682
1625 	.long	2493556375
1626 	.long	1050521105
1627 	.long	1046243251
1628 	.long	1062195323
1629 	.long	2593078846
1630 	.long	1049017717
1631 	.long	2763962276
1632 	.long	1060970161
1633 	.long	701480960
1634 	.long	1069894094
1635 	.long	3205862232
1636 	.long	1027177267
1637 	.long	0
1638 	.long	0
1639 	.long	2267016812
1640 	.long	1071015664
1641 	.long	0
1642 	.long	1072693248
1643 	.long	0
1644 	.long	4294967288
1645 	.long	1398462608
1646 	.long	1059820320
1647 	.long	0
1648 	.long	0
1649 	.long	26205983
1650 	.long	1058461213
1651 	.long	0
1652 	.long	0
1653 	.long	56226238
1654 	.long	1057044964
1655 	.long	2754706541
1656 	.long	1067875863
1657 	.long	2187799823
1658 	.long	1055634437
1659 	.long	790323742
1660 	.long	1067402587
1661 	.long	1372385848
1662 	.long	1054167831
1663 	.long	4097292716
1664 	.long	1065372654
1665 	.long	3348210357
1666 	.long	1052830099
1667 	.long	2442796466
1668 	.long	1064337602
1669 	.long	862608142
1670 	.long	1051347106
1671 	.long	170296152
1672 	.long	1062577219
1673 	.long	3755571428
1674 	.long	1049933343
1675 	.long	3614866008
1676 	.long	1061361670
1677 	.long	719978496
1678 	.long	1070185448
1679 	.long	1998842465
1680 	.long	1027220329
1681 	.long	0
1682 	.long	0
1683 	.long	3749156607
1684 	.long	1071048258
1685 	.long	0
1686 	.long	1072693248
1687 	.long	0
1688 	.long	4294967288
1689 	.long	1330165971
1690 	.long	1060367097
1691 	.long	0
1692 	.long	0
1693 	.long	217536623
1694 	.long	1059109098
1695 	.long	0
1696 	.long	0
1697 	.long	3492120849
1698 	.long	1057667827
1699 	.long	602185705
1700 	.long	1068194444
1701 	.long	760422958
1702 	.long	1056312597
1703 	.long	555127889
1704 	.long	1067545266
1705 	.long	3139784124
1706 	.long	1054987189
1707 	.long	3690544014
1708 	.long	1065666523
1709 	.long	95707915
1710 	.long	1053635428
1711 	.long	4003114407
1712 	.long	1064581412
1713 	.long	2034926231
1714 	.long	1052227513
1715 	.long	3759536023
1716 	.long	1063076341
1717 	.long	3826928214
1718 	.long	1050893819
1719 	.long	3837960785
1720 	.long	1061790379
1721 	.long	1526325248
1722 	.long	1070483918
1723 	.long	2356426521
1724 	.long	3172907104
1725 	.long	0
1726 	.long	0
1727 	.long	457728975
1728 	.long	1071088276
1729 	.long	0
1730 	.long	1072693248
1731 	.long	0
1732 	.long	4294967288
1733 	.long	1704352102
1734 	.long	3223426649
1735 	.long	0
1736 	.long	0
1737 	.long	2284589306
1738 	.long	1076258036
1739 	.long	0
1740 	.long	0
1741 	.long	2211264291
1742 	.long	3224142658
1743 	.long	0
1744 	.long	3221225472
1745 	.long	1441186365
1746 	.long	1077028579
1747 	.long	1431655765
1748 	.long	1074091349
1749 	.long	876943673
1750 	.long	3224837270
1751 	.long	2863311531
1752 	.long	3221924522
1753 	.long	236289504
1754 	.long	1077767485
1755 	.long	286331153
1756 	.long	1074860305
1757 	.long	2805473311
1758 	.long	3225598926
1759 	.long	95443718
1760 	.long	3222646875
1761 	.long	1160476131
1762 	.long	1078450742
1763 	.long	463583772
1764 	.long	1075552698
1765 	.long	0
1766 	.long	3220176896
1767 	.long	0
1768 	.long	0
1769 	.long	0
1770 	.long	1073741824
1771 	.long	0
1772 	.long	0
1773 	.long	0
1774 	.long	0
1775 	.long	0
1776 	.long	0
1777 	.long	3349892442
1778 	.long	3221773860
1779 	.long	0
1780 	.long	0
1781 	.long	3913197405
1782 	.long	1074501181
1783 	.long	0
1784 	.long	0
1785 	.long	2494034522
1786 	.long	3222222818
1787 	.long	1264738763
1788 	.long	3220568452
1789 	.long	1520293906
1790 	.long	1074899632
1791 	.long	1958936600
1792 	.long	1073411493
1793 	.long	2133649635
1794 	.long	3222535819
1795 	.long	4270740730
1796 	.long	3221058356
1797 	.long	1728930189
1798 	.long	1075224844
1799 	.long	1303998552
1800 	.long	1073799186
1801 	.long	618611933
1802 	.long	3222903903
1803 	.long	1769828046
1804 	.long	3221422190
1805 	.long	2200537986
1806 	.long	1075641421
1807 	.long	433361110
1808 	.long	1074105369
1809 	.long	719595600
1810 	.long	3219800832
1811 	.long	294527206
1812 	.long	1014656440
1813 	.long	0
1814 	.long	1073741824
1815 	.long	3811788216
1816 	.long	3218400550
1817 	.long	0
1818 	.long	0
1819 	.long	0
1820 	.long	0
1821 	.long	724322768
1822 	.long	3220364956
1823 	.long	0
1824 	.long	0
1825 	.long	643153048
1826 	.long	1072905816
1827 	.long	0
1828 	.long	0
1829 	.long	4285079458
1830 	.long	3220412206
1831 	.long	3912524733
1832 	.long	3220106631
1833 	.long	118362272
1834 	.long	1072952754
1835 	.long	4107767972
1836 	.long	1072827408
1837 	.long	2689502883
1838 	.long	3220460570
1839 	.long	946523347
1840 	.long	3220256414
1841 	.long	573204189
1842 	.long	1073001761
1843 	.long	581531518
1844 	.long	1072826391
1845 	.long	1386236526
1846 	.long	3220510607
1847 	.long	3718905905
1848 	.long	3220316471
1849 	.long	1145558140
1850 	.long	1073052673
1851 	.long	513572637
1852 	.long	1072861969
1853 	.long	716700048
1854 	.long	3219481016
1855 	.long	547126769
1856 	.long	3163007173
1857 	.long	0
1858 	.long	1072693248
1859 	.long	1097907398
1860 	.long	1071420120
1861 	.long	0
1862 	.long	0
1863 	.long	0
1864 	.long	0
1865 	.long	3422807297
1866 	.long	3219124495
1867 	.long	0
1868 	.long	0
1869 	.long	1151658053
1870 	.long	1071494715
1871 	.long	0
1872 	.long	0
1873 	.long	929607071
1874 	.long	3218829988
1875 	.long	1037049034
1876 	.long	3219520953
1877 	.long	2786928657
1878 	.long	1071215282
1879 	.long	1447406859
1880 	.long	1072265209
1881 	.long	3490952107
1882 	.long	3218574499
1883 	.long	3205232916
1884 	.long	3219452306
1885 	.long	1297344304
1886 	.long	1070977120
1887 	.long	1066110976
1888 	.long	1071946035
1889 	.long	3803721480
1890 	.long	3218354730
1891 	.long	1496754229
1892 	.long	3219290849
1893 	.long	2982550683
1894 	.long	1070773243
1895 	.long	4014441989
1896 	.long	1071736222
1897 	.long	419968236
1898 	.long	3219200695
1899 	.long	3451266538
1900 	.long	1015961163
1901 	.long	0
1902 	.long	1072693248
1903 	.long	2960267235
1904 	.long	1070745841
1905 	.long	0
1906 	.long	0
1907 	.long	0
1908 	.long	0
1909 	.long	1458794562
1910 	.long	3217882198
1911 	.long	0
1912 	.long	0
1913 	.long	2857777489
1914 	.long	1070137637
1915 	.long	0
1916 	.long	0
1917 	.long	1024359517
1918 	.long	3217360179
1919 	.long	2616040238
1920 	.long	3219066585
1921 	.long	1609024636
1922 	.long	1069675088
1923 	.long	2529240549
1924 	.long	1071836633
1925 	.long	1510128600
1926 	.long	3216923761
1927 	.long	2251697184
1928 	.long	3218737335
1929 	.long	1262761453
1930 	.long	1069142850
1931 	.long	1263091857
1932 	.long	1071190461
1933 	.long	3043383486
1934 	.long	3216368839
1935 	.long	2476932470
1936 	.long	3218325650
1937 	.long	3659995028
1938 	.long	1068669200
1939 	.long	855891755
1940 	.long	1070696894
1941 	.long	2583490354
1942 	.long	3218768505
1943 	.long	3062633575
1944 	.long	3161492271
1945 	.long	0
1946 	.long	1072693248
1947 	.long	2550940471
1948 	.long	1069938201
1949 	.long	0
1950 	.long	0
1951 	.long	0
1952 	.long	0
1953 	.long	2467582782
1954 	.long	3216740037
1955 	.long	0
1956 	.long	0
1957 	.long	162150096
1958 	.long	1068946420
1959 	.long	0
1960 	.long	0
1961 	.long	3702794237
1962 	.long	3216062800
1963 	.long	3631919291
1964 	.long	3218420574
1965 	.long	3456821413
1966 	.long	1068217218
1967 	.long	2031366438
1968 	.long	1071495745
1969 	.long	1596664020
1970 	.long	3215282929
1971 	.long	1509038701
1972 	.long	3218085291
1973 	.long	583171477
1974 	.long	1067510148
1975 	.long	3785344682
1976 	.long	1070618476
1977 	.long	2402036048
1978 	.long	3214559384
1979 	.long	3233018412
1980 	.long	3217396834
1981 	.long	411280568
1982 	.long	1066710556
1983 	.long	1065584192
1984 	.long	1069747896
1985 	.long	895247324
1986 	.long	3218303496
1987 	.long	500078909
1988 	.long	1013805133
1989 	.long	0
1990 	.long	1072693248
1991 	.long	729983843
1992 	.long	1068994194
1993 	.long	0
1994 	.long	0
1995 	.long	0
1996 	.long	0
1997 	.long	1512545955
1998 	.long	3215602695
1999 	.long	0
2000 	.long	0
2001 	.long	1127048698
2002 	.long	1067909459
2003 	.long	0
2004 	.long	0
2005 	.long	2300200450
2006 	.long	3214738415
2007 	.long	3593250296
2008 	.long	3217717209
2009 	.long	3009365544
2010 	.long	1066902117
2011 	.long	1127373050
2012 	.long	1071173457
2013 	.long	3046103305
2014 	.long	3213854947
2015 	.long	24583402
2016 	.long	3217207636
2017 	.long	4082511758
2018 	.long	1065914199
2019 	.long	3223889699
2020 	.long	1070020367
2021 	.long	548927984
2022 	.long	3212899404
2023 	.long	558065897
2024 	.long	3216433066
2025 	.long	680073315
2026 	.long	1064940726
2027 	.long	388873200
2028 	.long	1068944270
2029 	.long	3763679576
2030 	.long	3217651189
2031 	.long	1497360404
2032 	.long	3157194195
2033 	.long	0
2034 	.long	1072693248
2035 	.long	64931152
2036 	.long	1067729411
2037 	.long	0
2038 	.long	0
2039 	.long	0
2040 	.long	0
2041 	.long	1313038235
2042 	.long	3214229379
2043 	.long	0
2044 	.long	0
2045 	.long	1013878342
2046 	.long	1067152618
2047 	.long	0
2048 	.long	0
2049 	.long	3663426833
2050 	.long	3213208931
2051 	.long	3693284251
2052 	.long	3216602456
2053 	.long	650852232
2054 	.long	1065882376
2055 	.long	1996245381
2056 	.long	1071000265
2057 	.long	2008746170
2058 	.long	3212147845
2059 	.long	3055842593
2060 	.long	3216062494
2061 	.long	1495406348
2062 	.long	1064652437
2063 	.long	2269530157
2064 	.long	1069711235
2065 	.long	285563696
2066 	.long	3211060113
2067 	.long	1046897440
2068 	.long	3215189513
2069 	.long	233429731
2070 	.long	1063453151
2071 	.long	522045958
2072 	.long	1068476590
2073 	.long	2354785698
2074 	.long	3216586427
2075 	.long	1317599141
2076 	.long	3159915781
2077 	.long	0
2078 	.long	1072693248
2079 	.long	2828230105
2080 	.long	1065606626
2081 	.long	0
2082 	.long	0
2083 	.long	0
2084 	.long	0
2085 	.type	Ctable,@object
2086 	.size	Ctable,5632
2087 	.align 16
2088 MASK_35:
2089 	.long	4294705152
2090 	.long	4294967295
2091 	.long	0
2092 	.long	0
2093 	.type	MASK_35,@object
2094 	.size	MASK_35,16
2095 	.align 16
2096 Q_11:
2097 	.long	3103673719
2098 	.long	1065509018
2099 	.type	Q_11,@object
2100 	.size	Q_11,8
2101 	.space 8, 0x00 	# pad
2102 	.align 16
2103 Q_9:
2104 	.long	3213130307
2105 	.long	1066820768
2106 	.type	Q_9,@object
2107 	.size	Q_9,8
2108 	.space 8, 0x00 	# pad
2109 	.align 16
2110 Q_7:
2111 	.long	1388628139
2112 	.long	1068212666
2113 	.type	Q_7,@object
2114 	.size	Q_7,8
2115 	.space 8, 0x00 	# pad
2116 	.align 16
2117 Q_5:
2118 	.long	285812550
2119 	.long	1069617425
2120 	.type	Q_5,@object
2121 	.size	Q_5,8
2122 	.space 8, 0x00 	# pad
2123 	.align 16
2124 Q_3:
2125 	.long	1431655954
2126 	.long	1070945621
2127 	.type	Q_3,@object
2128 	.size	Q_3,8
2129 	.space 8, 0x00 	# pad
2130 	.align 16
2131 PI_INV_TABLE:
2132 	.long	0
2133 	.long	0
2134 	.long	2734261102
2135 	.long	1313084713
2136 	.long	4230436817
2137 	.long	4113882560
2138 	.long	3680671129
2139 	.long	1011060801
2140 	.long	4266746795
2141 	.long	3736847713
2142 	.long	3072618042
2143 	.long	1112396512
2144 	.long	105459434
2145 	.long	164729372
2146 	.long	4263373596
2147 	.long	2972297022
2148 	.long	3900847605
2149 	.long	784024708
2150 	.long	3919343654
2151 	.long	3026157121
2152 	.long	965858873
2153 	.long	2203269620
2154 	.long	2625920907
2155 	.long	3187222587
2156 	.long	536385535
2157 	.long	3724908559
2158 	.long	4012839307
2159 	.long	1510632735
2160 	.long	1832287951
2161 	.long	667617719
2162 	.long	1330003814
2163 	.long	2657085997
2164 	.long	1965537991
2165 	.long	3957715323
2166 	.long	1023883767
2167 	.long	2320667370
2168 	.long	1811636145
2169 	.long	529358088
2170 	.long	1443049542
2171 	.long	4235946923
2172 	.long	4040145953
2173 	.type	PI_INV_TABLE,@object
2174 	.size	PI_INV_TABLE,164
2175 	.space 12, 0x00 	# pad
2176 	.align 16
2177 PI_4:
2178 	.long	0
2179 	.long	1072243195
2180 	.long	1175561766
2181 	.long	1048908043
2182 	.type	PI_4,@object
2183 	.size	PI_4,16
2184 	.align 8
2185 QQ_2:
2186 	.long	1734816687
2187 	.long	1026746297
2188 	.type	QQ_2,@object
2189 	.size	QQ_2,8
2190 	.align 8
2191 ONE:
2192 	.long	0
2193 	.long	1072693248
2194 	.type	ONE,@object
2195 	.size	ONE,8
2196 	.align 8
2197 TWO_POW_55:
2198 	.long	0
2199 	.long	1130364928
2200 	.type	TWO_POW_55,@object
2201 	.size	TWO_POW_55,8
2202 	.align 8
2203 TWO_POW_M55:
2204 	.long	0
2205 	.long	1015021568
2206 	.type	TWO_POW_M55,@object
2207 	.size	TWO_POW_M55,8
2208 	.align 4
2209 NEG_ZERO:
2210 	.long	0
2211 	.long	2147483648
2212 	.type	NEG_ZERO,@object
2213 	.size	NEG_ZERO,8
2214 	.data
2215 	.section .note.GNU-stack, "",@progbits
2216 // -- Begin DWARF2 SEGMENT .eh_frame
2217 	.section .eh_frame,"a",@progbits
2218 .eh_frame_seg:
2219 	.align 1
2220 	.4byte 0x00000014
2221 	.8byte 0x00527a0100000000
2222 	.8byte 0x08070c1b01107801
2223 	.4byte 0x00000190
2224 	.4byte 0x0000002c
2225 	.4byte 0x0000001c
2226 	.4byte ..___tag_value_tan.1-.
2227 	.4byte ..___tag_value_tan.9-..___tag_value_tan.1
2228 	.2byte 0x0400
2229 	.4byte ..___tag_value_tan.3-..___tag_value_tan.1
2230 	.4byte 0x0283100e
2231 	.byte 0x04
2232 	.4byte ..___tag_value_tan.5-..___tag_value_tan.3
2233 	.2byte 0x200e
2234 	.byte 0x04
2235 	.4byte ..___tag_value_tan.6-..___tag_value_tan.5
2236 	.4byte 0x04c3100e
2237 	.4byte ..___tag_value_tan.8-..___tag_value_tan.6
2238 	.2byte 0x080e
2239 # End
2240

Last Index update Wed Jun 14 08:59:42 CST 2023