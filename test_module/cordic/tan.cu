#include <iostream>

const int N = 1024; // 假设你有一个大小为1024的数组
// CUDA add function
__global__ void add(float* a, float* b, float* c) {
    int idx = threadIdx.x + blockIdx.x * blockDim.x;
    // 确保索引没有超出数组边界（假设a、b和c有相同的长度）
    if (idx == 0) {
        double pi = 3.141592653589793;
        float rad = pi/2 - 1e-7;
        c[idx] = tanf(rad);
    } else {
        c[idx] = 0;
    }
}

// 主机端调用CUDA add函数的示例
int main() {

    float a[N], b[N], c[N];   // 定义主机端指针
    float *d_a, *d_b, *d_c; // 定义设备端指针

    for(int i = 0; i < N; i++){
        a[i] = i;
        b[i] = i;
        c[i] = 0;
    }
    // 分配内存
    cudaMalloc(&d_a, N * sizeof(float));
    cudaMalloc(&d_b, N * sizeof(float));
    cudaMalloc(&d_c, N * sizeof(float));

    // 将主机数据复制到设备
    cudaMemcpy(d_a, a, N * sizeof(float), cudaMemcpyHostToDevice);
    cudaMemcpy(d_b, b, N * sizeof(float), cudaMemcpyHostToDevice);


    int blockSize = 1024;
    int gridSize = 1;

    // 调用CUDA kernel函数
    add<<<gridSize, blockSize>>>(d_a, d_b, d_c);

    // 处理可能的CUDA错误
    cudaError_t err = cudaGetLastError();
    if (err != cudaSuccess) {
        // printf("CUDA error: %s\n", cudaGetErrorString(err));
        return -1;
    }

    // 将结果从设备复制回主机
    cudaMemcpy(c, d_c, N * sizeof(float), cudaMemcpyDeviceToHost);
    for(int i = 0; i < 16; i++){
        std::cout << "index: " << i << " out: " << c[i] << std::endl;
    }
    // 释放设备内存
    cudaFree(d_a);
    cudaFree(d_b);
    cudaFree(d_c);

    // 这里省略了对数组a、b在主机端初始化以及检查计算结果的部分...

    return 0;
}
