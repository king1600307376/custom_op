import pycuda.autoinit
import pycuda.driver as drv
from pycuda.compiler import SourceModule
import numpy as np
import time


def golden_byte_(x, y, s):
    tmp64 = (y << 32) | x
    print(tmp64)


def test_cuda_demo():
    st = """
        __global__ void demo(unsigned int *out, float *theta)
        {
            unsigned int a = 0x12345678;
            unsigned int b = 0x9abcdef0;
            unsigned int c = 0xc080404;
            out[0] = __byte_perm(a, b, c);
        }
    """
    mod = SourceModule(st)
    thread_num = 32
    block_size = (thread_num, 1, 1)
    grid = (1, 1, 1)
    func = mod.get_function("demo")
    src0_shape = (thread_num,)
    src0 = np.ones(src0_shape, dtype=np.float32) * (np.pi / 4)
    dst = np.zeros(src0_shape, dtype=np.uint32)
    print(src0)
    func(drv.Out(dst), drv.In(src0), block=block_size, grid=grid)
    """
    a =    0001 0010 0011 0100 0101 0110 0111 1000
    b =    1001 1010 1011 1100 1101 1110 1111 0000
    s =         1100 0000 1000 0000 0100 0000 1111
    out =0b0111 1000 1111 0000 0111 1000 0111 1000
           0110 1000 1111 0000 0111 1000 1001 1010
    
    a =   0x12345678  0x12345678
    b =   0x9abcdef0  0x9abcdef0
    s =   0x0c080400  0x0c08040f 0x0c08040c 0x0c080408 0x0c080404
    out = 0x78f07878  0x78f0789a 0x78f078f0 0x78f07878 0x78f078f0
    
    
    """
    a = 0x12345678
    b = 0x9abcdef0
    c = 0b1010000010110000110000001101
    golden_byte_(a, b, c)
    return dst


def test_case1():
    pass


if __name__ == '__main__':
    out_arr = test_cuda_demo()
    print(out_arr[:32])
