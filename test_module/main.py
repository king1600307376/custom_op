import time
from tqdm import tqdm
from multiprocessing import Pool
from remote_manager.remote_manager import RemoteManager

config = {
    "camodel": [
        {"username": "root", "ip": "19", "passwd": "root", "case_rate": 1, "host_path": "/home", "is_run": False},
    ],
    "esl_model": [
        {"username": "root", "ip": "11", "passwd": "22", "case_rate": 1,
         "host_path": "/home", "is_run": True},
    ],
    "cuda": [
        {"username": "root", "ip": "21", "passwd": "root", "case_rate": 1, "host_path": "/home", "is_run": False},
    ],
}


def run_one_host(item):
    if item["is_run"] is True:
        if item["ip"] == "19":
            for _ in range(19):
                time.sleep(1)
        elif item["ip"] == "11":
            with RemoteManager(item["ip"], 22, item["username"], item["passwd"]) as rm:
                rm.run_command("mkdir -p /home/jjc/tmp")
                rm.put("/home/jjc/tmp", "/home/jjc/put_tmp", recursive=True)
                rm.get("/home/jjc/put_tmp", "/home/jjc/tmp", recursive=True)
            for i in tqdm(range(20)):
                time.sleep(1)
        elif item["ip"] == "21":
            for _ in range(21):
                time.sleep(1)


def run_all():
    host_count = 0
    for dic in config.values():
        host_count += len(dic)
    p = Pool(processes=host_count)
    for seq in config.values():
        for item in seq:
            p.apply_async(run_one_host, args=(item,))

    p.close()
    p.join()


if __name__ == '__main__':
    run_all()
