import os
import paramiko
from scp import SCPClient


class RemoteManager:
    def __init__(self, host, port, username, password, conn_timeout=10, scp_timeout=1000):
        self.host = host
        self.port = port
        self.username = username
        self.password = password
        self.conn_timeout = conn_timeout
        self.scp_timeout = scp_timeout
        self.client = paramiko.SSHClient()
        self.scp = None

    def connect(self):
        self.client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        self.client.connect(self.host, self.port, self.username, self.password, timeout=self.conn_timeout)
        self.scp = SCPClient(self.client.get_transport(), socket_timeout=self.scp_timeout)

    def close(self):
        self.client.close()

    def run_command(self, command):
        stdin, stdout, stderr = self.client.exec_command(command)
        output = ""
        while True:
            chunk = stdout.channel.recv(1024)
            if not chunk:
                break
            output += chunk.decode()
        return output

    def get(self, local_path, remote_path, remote_file_name="", recursive=True):
        if self.scp is not None:
            print(f"remote get dir {remote_path}")
            self.scp.get(os.path.join(remote_path, remote_file_name), local_path, recursive=recursive)
        else:
            print("scp is None")

    def put(self, remote_path, local_path, local_file_name="", recursive=True):
        if self.scp is not None:
            print(f"remote put dir {remote_path}")
            self.scp.put(os.path.join(local_path, local_file_name), remote_path, recursive=recursive)
        else:
            print("scp is None")

    def __enter__(self):
        self.connect()
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.close()


if __name__ == '__main__':
    with RemoteManager('192.168.0.88', 22, '33', '22') as rm:
        print(rm.run_command("ls"))
        rm.put("/home/jjc/tmp", "/home/jjc/put_tmp", recursive=True)
        rm.get("/home/jjc/put_tmp", "/home/jjc/tmp", recursive=True)
