def custom_hash(key):
    return key % 5


def custom_hash_2(key):
    return (key // 10) % 5


class CuckooHashTable:
    def __init__(self, n):
        self.n = n
        self.arr = [-1 for _ in range(self.n)]
        self.max_loop = 10
        self.insert_count = 0

    def insert(self, key):
        lookup_val = self._lookup(key)
        if lookup_val == key:
            return True
        hash_index_1 = custom_hash(key)

        if self.arr[hash_index_1] == -1:
            self.arr[hash_index_1] = key
            return True
        else:
            old_val = self.arr[hash_index_1]
            self.arr[hash_index_1] = key
            hash_index_2 = custom_hash_2(old_val)
            if self.arr[hash_index_2] == -1:
                self.arr[hash_index_2] = old_val
                return True
            else:
                old_val_2 = self.arr[hash_index_2]
                self.arr[hash_index_2] = old_val
                while self.insert_count < self.max_loop:
                    hash_index_2 = custom_hash_2(old_val_2)
                    if self.arr[hash_index_2] == -1:
                        self.arr[hash_index_2] = old_val_2
                        return True
                    self.insert_count += 1

    def lookup(self, key):
        if self._lookup(key) > -1:
            return
        raise RuntimeError("Key not found")

    def _lookup(self, key):
        lookup_val = self.arr[custom_hash(key)]
        if lookup_val != -1:
            return lookup_val
        print(custom_hash_2(key), key)
        lookup_val = self.arr[custom_hash_2(key)]
        if lookup_val != -1:
            return lookup_val
        return -1

    def expansion(self):
        """
        扩容
        """
        self.arr.extend([-1 for _ in range(self.n)])


if __name__ == '__main__':
    table = CuckooHashTable(10)
    for i in [129, 875, 312]:
        table.insert(i)
    table.insert(234)
    print(table.arr)
