import unittest
import numpy as np
import tensorflow as tf


def reduce_mul(ls):
    val = 1
    for i in ls:
        val *= i
    return val


def gen_gather_data(dst, src, indices, axis=0, batch_dims=0):
    dst = []
    for i in range(len(indices)):
        tmp = []
        for j in range(len(indices[0])):
            tmp.append(src[indices[i][j]])
        dst.append(tmp)
    return np.array(dst, dtype=src.dtype)


def gen_gather_with_tf(x, indices, axis=0, batch_dims=0):
    src = tf.constant(x)
    # index = tf.constant(indices)
    out = tf.gather(src, indices=indices, axis=axis, batch_dims=batch_dims)
    return out


def cmp_data(out, golden_data):
    rate = 0.0001
    err_count = 0
    out = np.array(out).reshape(-1)
    golden_data = np.array(golden_data).reshape(-1)
    for i, v in enumerate(golden_data):
        if float(np.abs(golden_data[i] - out[i])) > rate:
            err_count += 1

    assert err_count == 0, "out not eq golden data"


def cce_gather(dst, src, indices):
    """
    src (3,) indices (2, 3) dst (2, 3)
    src (3, 4) indices (2, 3) dst (2, 3, 4)
    src (2, 3, 4) indices (2, 3) dst (2, 3, 3, 4)
    src (2, 2, 3, 4) indices (2, 3) dst (2, 3, 2, 3, 4)
    src (3, 4) indices (2,) dst (2, 4)
    src (2, 3, 4) indices (2,) dst (2, 3, 4)
    src (2, 2, 3, 4) indices (2,) dst (2, 2, 3, 4)
    """
    src_shape = src.shape
    indices_shape = indices.shape
    dst_shape = dst.shape
    src = src.reshape(-1)
    indices = indices.reshape(-1)
    dst = dst.reshape(-1)
    if len(src_shape) == 1:
        for i in range(indices_shape[0]):
            for j in range(indices_shape[1]):
                index_dim0 = i
                dim0_offset = index_dim0 * indices_shape[1]
                index_dim1 = j
                dst[dim0_offset + index_dim1] = src[indices[dim0_offset + index_dim1]]
    elif len(src_shape) == 2:
        src_last_dim = src_shape[1]

        for i in range(indices_shape[0]):
            for j in range(indices_shape[1]):
                index_dim0 = i
                dim0_offset = index_dim0 * indices_shape[1]
                index_dim1 = j

                for k in range(src_last_dim):
                    dst[dim0_offset * src_last_dim + index_dim1 * src_last_dim + k] = \
                        src[indices[dim0_offset + index_dim1] * src_last_dim + k]
    elif len(src_shape) == 3:
        src_dim1 = src_shape[1]
        src_dim2 = src_shape[2]
        print(src_dim2)
        for i in range(indices_shape[0]):
            for j in range(indices_shape[1]):
                index_dim0 = i
                dim0_offset = index_dim0 * indices_shape[1]
                index_dim1 = j

                src_dims = src_dim1 * src_dim2
                for k in range(src_dim1):
                    for f in range(src_dim2):
                        dst[dim0_offset * src_dims + index_dim1 * src_dims + k * src_dim2 + f] = \
                            src[indices[dim0_offset + index_dim1] * src_dims + k * src_dim2 + f]
    """
    src_shape: dim0, dim1, dim2, dim3
    indices_shape: i_dim0, i_dim1, i_dim2 ...
    dst_shape: i_dim0, i_dim1, ... , dim1, dim2, ...
    
    indices offset = i*(dim1*...) + j
    dst offset = dim0_offset*(dim1*dim2*...) + index_dim1*(dim1*dim2*...) + k*(dim2*...) + f
    src offset = indices[dim0_offset + index_dim1]*(dim1*dim2*...) + k*(dim2*...) + f
    """
    return dst.reshape(dst_shape)


def cce_gather_auto_shape(dst, src, indices):
    src_shape = src.shape
    indices_shape = indices.shape
    dst_shape = dst.shape
    src = src.reshape(-1)
    indices = indices.reshape(-1)
    dst = dst.reshape(-1)

    # if len(src_shape) == 1:
    #     dim0, = src_shape
    #     i_dim0, i_dim1 = indices_shape
    #     for threadIdx in range(i_dim0 * i_dim1):
    #         index_i = threadIdx // i_dim1  # indices dim0 i
    #         index_j = threadIdx - index_i * i_dim1  # indices dim1 j
    #         s_i = threadIdx // 1
    #         dst[index_i * i_dim1 + index_j] = src[indices[index_i * i_dim1 + index_j]]
    #     return dst.reshape(dst_shape)
    # if len(src_shape) == 2:
    #     dim0, dim1 = src_shape
    #     i_dim0, i_dim1 = indices_shape
    #     for threadIdx in range(i_dim0 * i_dim1 * dim1):
    #         index_i = threadIdx // dim1 // i_dim1
    #         index_j = threadIdx // dim1 - index_i * i_dim1
    #         s_i = threadIdx // dim1
    #         s_j = threadIdx % dim1
    #         print(threadIdx, index_i, index_j, s_i, s_j)
    #         dst[(index_i * i_dim1 + index_j) * dim1 + s_j] = \
    #             src[indices[index_i * i_dim1 + index_j] * dim1 + s_j]
    #     return dst.reshape(dst_shape)
    # if len(src_shape) == 3:
    #     dim0, dim1, dim2 = src_shape
    #     i_dim0, i_dim1 = indices_shape
    #     for threadIdx in range(i_dim0 * i_dim1 * dim1 * dim2):
    #         index_i = threadIdx // (dim1 * dim2) // i_dim1
    #         index_j = threadIdx // (dim1 * dim2) - index_i * i_dim1
    #         s_i = threadIdx // (dim1 * dim2)
    #         s_j = threadIdx // dim2 % dim1
    #         s_k = threadIdx % dim2
    #         print(threadIdx, index_i, index_j, s_i, s_j, s_k)
    #         dst[(index_i * i_dim1 + index_j) * dim1 * dim2 + s_j * dim2 + s_k] = \
    #             src[indices[index_i * i_dim1 + index_j] * dim1 * dim2 + s_j * dim2 + s_k]
    #     return dst.reshape(dst_shape)
    # if len(src_shape) == 4:
    #     dim0, dim1, dim2, dim3 = src_shape
    #     i_dim0, i_dim1 = indices_shape
    #     for threadIdx in range(i_dim0 * i_dim1 * dim1 * dim2 * dim3):
    #         index_i = threadIdx // (dim1 * dim2 * dim3) // i_dim1
    #         index_j = threadIdx // (dim1 * dim2 * dim3) - index_i * i_dim1
    #         s_i = threadIdx // (dim1 * dim2 * dim3)
    #         s_j = threadIdx // (dim2 * dim3) % dim1
    #         s_k = threadIdx // dim3 % dim2
    #         s_v = threadIdx % dim3
    #         dst[(index_i * i_dim1 + index_j) * dim1 * dim2 * dim3 + s_j * dim2 * dim3 + s_k * dim3 + s_v] = \
    #             src[indices[index_i * i_dim1 + index_j] * dim1 * dim2 * dim3 + s_j * dim2 * dim3 + s_k * dim3 + s_v]
    #     return dst.reshape(dst_shape)
    # dim0, dim1, dim2, dim3 = src_shape
    i_dim0, i_dim1 = indices_shape
    dst_n = i_dim0 * i_dim1 * int(np.prod(src_shape[1:]))
    src_n = reduce_mul(src_shape[1:])

    for threadIdx in range(dst_n):
        index_i = threadIdx // src_n // i_dim1
        index_j = threadIdx // src_n - index_i * i_dim1
        # s_i = threadIdx // src_n
        # s_j = threadIdx - s_i * src_n
        # s_k = threadIdx - (s_i * src_n + s_j * reduce_mul(src_shape[2:]))
        # s_v = threadIdx - (s_i * src_n + s_j * reduce_mul(src_shape[2:]) + s_k * reduce_mul(src_shape[3:]))

        # s_i = threadIdx // src_n
        c = 0
        for offset in range(1, len(src_shape)):
            c += (threadIdx // (reduce_mul(src_shape[offset + 1:])) % src_shape[offset]) * reduce_mul(
                src_shape[offset + 1:])
        print(threadIdx)
        index_pos = index_i * i_dim1 + index_j
        dst[index_pos * src_n + c] = \
            src[indices[index_pos] * src_n + c]
    return dst.reshape(dst_shape)


class TestGather(unittest.TestCase):
    def test_gather(self):
        a = np.arange(0, 12, 1).astype(np.float32).reshape((3, 4))
        b = np.random.randint(0, 3, size=(2, 3), dtype=np.int64)
        c = np.zeros((2, 3, 4), np.float32)
        print(f"src:{a}")
        print(f"index:{b}")
        np_out = gen_gather_data(c, a, b)
        print(f"np_out: {np_out}")
        print(f"np_out.shape: {np_out.shape}")
        tf_out = gen_gather_with_tf(a, b)
        print(f"tf_out: {tf_out}")
        print(f"tf_out.shape: {tf_out.shape}")

    def test_cce_gather_with_tf(self):
        ls = [((3,), (2, 3)), ((3, 4), (2, 3)), ((2, 3, 4), (2, 3)), ((2, 2, 3, 4), (2, 3)),
              ((3, 4), (2,)), ((2, 3, 4), (2,)), ((2, 2, 3, 4), (2,))]
        i = 0
        start = 1
        end = start + 3
        st = ""
        for src_shape, indices_shape in ls[start:end]:
            # if i != 0:
            #     continue
            a = np.arange(0, int(np.prod(src_shape)), 1).astype(np.float32).reshape(src_shape)
            b = np.random.randint(0, 2, size=indices_shape, dtype=np.int64)
            c = np.zeros((*indices_shape, *src_shape[1:]), np.float32)
            print(f"src:{a}")
            print(f"index:{b}")
            np_out = cce_gather_auto_shape(c, a, b)
            print()
            print(f"np_out: {np_out}")
            print(f"np_out.shape: {np_out.shape}")
            print()
            tf_out = gen_gather_with_tf(a, b)
            print(f"tf_out: {tf_out}")
            print(f"tf_out.shape: {tf_out.shape}")
            i += 0
            st += f"src {src_shape} indices {indices_shape} dst {tf_out.shape}\n"
            # assert (np.array(np_out) == np.array(tf_out)).all() is True
            cmp_data(np_out, tf_out)
            # break
        print()
        print(st)


if __name__ == '__main__':
    unittest.main()

