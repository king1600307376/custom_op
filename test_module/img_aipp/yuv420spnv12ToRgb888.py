import numpy as np


def yuv_rgb888():
    dst_h, dst_w, c = (2, 8, 3)
    src = np.random.randint(0, 255, (24, ), np.uint8)
    dst = np.zeros((dst_h, dst_w, c), dtype=np.uint8).reshape(-1)
    # y内存长度与uv内存长度比为2:1
    y_len = len(src)//3*2
    uv_len = y_len // 2
    # 每8个y为一组
    for i in range(dst_h):
        y1, y2, y3, y4, y5, y6, y7, y8 = src[i*uv_len:i*uv_len+uv_len]
        u1, v1, u2, v2 = src[y_len+i*(uv_len//2):y_len+i*(uv_len//2)+uv_len//2]
        y_ls = [y1, y2, y3, y4, y5, y6, y7, y8]
        u_ls = [u1, u1, u2, u2, u1, u1, u2, u2]
        v_ls = [v1, v1, v2, v2, v1, v1, v2, v2]
        for j in range(8):
            y = y_ls[j]
            u = u_ls[j]
            v = v_ls[j]
            # 转换方程
            r = y + 1.4075 * v
            g = y - 0.3455 * u - 0.7169 * v
            b = y + 1.779 * u
            dst[i*3*8+j*3:i*3*8+j*3+3] = np.array([r, g, b], np.uint8)
    print(dst)


if __name__ == '__main__':
    yuv_rgb888()
