import numpy as np
from PIL import Image


def read_img(file_name):
    img = Image.open(file_name)
    img_arr = np.array(img)
    print(f"{file_name}: {img_arr.shape, img_arr.dtype}")
    print(img_arr[:2, :2, :])
    print(img_arr.reshape(-1)[:6])


if __name__ == '__main__':
    ls = ["img_bmp.bmp", "img_jpg.jpg", "img_png.png", "img_xbm.xbm"]
    for name in ls:

        read_img(name)
        break
