import sys

import pycuda.autoinit
import pycuda.driver as drv
from pycuda.compiler import SourceModule
import numpy as np
import time


def test_cuda_demo():
    st = """
        __global__ void demo(float *a, float *b, float *c, int size)
        {
            c[threadIdx.x] = 11;
            for (int i=threadIdx.x;i<size;i+=size)
            {
                //c[i] = a[i] + b[i];
                
            }
        }
    """
    mod = SourceModule(st)
    thread_num = 1024
    block_size = (thread_num, 1, 1)
    grid = (1, 1, 1)
    func = mod.get_function("demo")
    src0_shape = (thread_num,)
    src1_shape = (thread_num,)
    src0 = np.ones(src0_shape, dtype=np.float32)
    src1 = np.ones(src1_shape, dtype=np.float32)
    dst = np.zeros(src0_shape, dtype=np.float32)
    func(drv.In(src0), drv.In(src1), drv.Out(dst), np.int32(thread_num), block=block_size, grid=grid)
    print(dst)


def test_img_gm_to_core():
    h, w = 1024, 1024
    img = np.arange(0, h * w).astype(np.int32).reshape(h, w)
    dst_h, dst_w = 224, 224
    return img_cro(img, 1044, w, dst_h, dst_w)


def img_cro(img, start, src_w, dst_h, dst_w):
    img = img.reshape(-1)
    gap = src_w - dst_w
    dst = np.zeros((dst_h, dst_w), img.dtype).reshape(-1)
    extent = (dst_h - 1) * gap + (dst_h * dst_w)
    # print(img)
    # print(img[1044])
    # print(start, start+extent, img[start:start+extent])
    return gm_to_core(dst, img[start:start+extent], dst_h, dst_w, gap)


def gm_to_core(dst, src, burst_len, burst, gap):
    if len(src.shape) != 1:
        raise RuntimeError("src shape should be (1, )")
    # print(src)
    for row in range(burst_len):
        dst[row * burst:row * burst + burst] = src[row * (burst + gap):row * (burst + gap) + burst]
    return dst


if __name__ == '__main__':
    out = test_img_gm_to_core()
    print(out[:32], out[-32:])
    # if sys.argv[1] in globals():
    #     globals()[sys.argv[1]]()
    # else:
    #     print(f"params {sys.argv[1]} is invalid")
